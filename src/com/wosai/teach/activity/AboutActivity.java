package com.wosai.teach.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.wosai.teach.R;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月3日 上午11:53:21
 * @desc :
 */
@ContentView(R.layout.activity_about)
public class AboutActivity extends AppActivity {

	@ViewInject(R.id.default_tv)
	private TextView tv;

	@ViewInject(R.id.about_title)
	private TextView title;

	@ViewInject(R.id.about_copyright)
	private TextView copyright;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTopTitle("关于沃赛");
		setmPageName("关于沃赛");
		enableBack(true);
		initView();
	}

	private void initView() {
		StringBuffer sb = new StringBuffer();
		sb.append("        沃赛仿真就是利用移动互联网手机应用和平板应用，包容传统PC应用的跨平台技术，结合云计算，云存储，图形图像等多种计算机技术来模拟仪器设备，实验等真实教学内容等，学生模拟扮演某一角色进行技能训练的一种教学方法。仿真教育能在很大程度上弥补客观条件的不足，为学生提供近似真实的训练环境，提高学生职业技能。沃赛仿真充分吸收互联网思维和年轻人元素，让仿真趣味化，游戏化，实现寓教于乐的互联网+教育。并融合预习、讲课、测验、评估于一体，实现围绕仿真教育的在线课堂教学平台，充分满足现代教育需求。");
		tv.setText(sb.toString());
		title.setText("杭州沃赛科技有限公司");
		copyright.setText("Copyright ©wosaitech.com  All Rights Reserved.");
	}
}
