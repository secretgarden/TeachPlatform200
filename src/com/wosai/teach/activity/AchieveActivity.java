package com.wosai.teach.activity;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.wosai.teach.R;
import com.wosai.teach.adapter.AchieveAdapter;
import com.wosai.teach.cst.C;
import com.wosai.teach.dialog.NewProgressDialog;
import com.wosai.teach.dto.JsonResult;
import com.wosai.teach.entity.Honor;
import com.wosai.teach.entity.User;
import com.wosai.teach.view.NoScrollListView;

@ContentView(R.layout.activity_achieve)
public class AchieveActivity extends AppActivity {

	private List<Honor> honorList;

	private User user;

	private AchieveAdapter adapter;

	@ViewInject(R.id.achieve_list)
	private NoScrollListView listMsg;

	private NewProgressDialog progressdialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setmPageName("成就");
		setTopTitle("成就");
		enableBack(true);
		user = db.findFirst(User.class);
		if (user == null) {
			Toast.makeText(getApplicationContext(), "请先登录", Toast.LENGTH_SHORT)
					.show();
			Intent it = new Intent(AchieveActivity.this, LoginActivity.class);
			startActivity(it);
		}else{
			loadHonor();
		}
		
	}

	private void loadHonor() {
		progressdialog = NewProgressDialog
				.showProgress(this, "努力加载中...", false);

		String url = C.HTTP_URL_HONOR_LIST;
		url = url.replace("{userId}", user.getUserId() + "");
		System.out.println(url);

		HttpUtils http = new HttpUtils();
		http.send(HttpRequest.HttpMethod.GET, url,
				new RequestCallBack<String>() {

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						JsonResult jr = JSON.parseObject(responseInfo.result,
								JsonResult.class);
						if (jr.getResult() == 0) {
							honorList = JSON.parseArray(jr.getObject()
									.toString(), Honor.class);
							db.deleteAll(Selector.from(Honor.class));
							db.saveAll(honorList);
						} else {
							honorList = db.findAll(Selector.from(Honor.class));
						}
						refreshList();
					}

					@Override
					public void onFailure(HttpException error, String msg) {
						honorList = db.findAll(Selector.from(Honor.class));
						refreshList();
					}

				});
	}

	private void refreshList() {
		if (adapter == null) {
			adapter = new AchieveAdapter(this, honorList, db);
			listMsg.setAdapter(adapter);
			adapter.setListView(listMsg);
		} else {
			adapter.setList(honorList);
			adapter.notifyDataSetChanged();
		}
		adapter.refreshHeight();
		if (progressdialog != null) {
			progressdialog.dismiss();
		}
	}

}
