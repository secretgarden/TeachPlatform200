package com.wosai.teach.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.wosai.teach.R;
import com.wosai.teach.cst.AppCst;
import com.wosai.teach.dialog.NetAvaiableDialog;
import com.wosai.teach.entity.User;
import com.wosai.teach.utils.AppUtil;
import com.wosai.teach.utils.BitmapHelp;

public class AppActivity extends BaseActivity implements OnClickListener {
	private FrameLayout mFlRoot;
	private RelativeLayout mRlBack;
	private RelativeLayout mRlRefresh;
	private RelativeLayout mRlHead;
	private RelativeLayout mRLshare;
	private RelativeLayout mRLdel;
	private RelativeLayout mRLpublish;
	private RelativeLayout mRlSearch;
	private TextView mTxtTitle;
	private RelativeLayout mRlMessage;
	private ProgressBar mPbProgress;
	private TextView mTxtCount;
	private MessageReceiver mMsgReceiver;
	private boolean mTipExit;
	private boolean mExit;
	private OnRefreshListener mRefreshListener;
	private OnBackListener mBackListener;
	private NetAvaiableDialog mNetAvaiableDialog;
	private OnShareListener mShareListener;
	private OnDelListener mDelListener;
	private OnPublishListener mPublishListener;
	private OnSearchListener mSearchListener;

	private RelativeLayout mHeadTop;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.activity_base2);

		mHeadTop = (RelativeLayout) findViewById(R.id.head_top);

		// 返回
		mRlBack = (RelativeLayout) findViewById(R.id.base_id_back);
		mRlBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mBackListener != null) {
					mBackListener.onBack();
					finish();
				} else {
					finish();
				}
			}
		});

		// 系统消息
		mRlMessage = (RelativeLayout) findViewById(R.id.base_id_message);
		mTxtCount = (TextView) findViewById(R.id.base_id_count);
		mRlMessage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// Intent intent = new Intent(BaseActivity.this,
				// MessageActivity.class);
				// startActivity(intent);
			}
		});

		// 分享
		mRLshare = (RelativeLayout) findViewById(R.id.base_id_share);
		mRLshare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mShareListener != null) {
					mShareListener.onShare();
				}
			}
		});

		// 删除
		mRLdel = (RelativeLayout) findViewById(R.id.base_id_del);
		mRLdel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mDelListener != null) {
					mDelListener.onDel();
				}
			}
		});

		// 发表（同学圈）
		mRLpublish = (RelativeLayout) findViewById(R.id.base_id_publish);
		mRLpublish.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mPublishListener != null) {
					mPublishListener.onPublish();
				}
			}
		});

		// 搜索
		mRlSearch = (RelativeLayout) findViewById(R.id.base_id_search);
		mRlSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mSearchListener != null) {
					mSearchListener.onSearch();
				}
			}
		});

		// 进度
		mPbProgress = (ProgressBar) findViewById(R.id.base_id_progress);

		IntentFilter filter = new IntentFilter();
		filter.addAction(AppCst.MSG_ACTION);
		mMsgReceiver = new MessageReceiver();
		registerReceiver(mMsgReceiver, filter);

		// 标题
		mTxtTitle = (TextView) findViewById(R.id.base_id_title);

		// 刷新
		mRlRefresh = (RelativeLayout) findViewById(R.id.base_id_refresh);
		mRlRefresh.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mRefreshListener != null)
					mRefreshListener.onRefresh();
			}
		});

		// 头像
		mRlHead = (RelativeLayout) findViewById(R.id.base_id_head);
		mRlHead.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

			}
		});

		// 内容
		mFlRoot = (FrameLayout) findViewById(R.id.base_id_root);

		// 无网络对话框
		mNetAvaiableDialog = new NetAvaiableDialog(AppActivity.this);
		showUserIcon();

		ViewUtils.inject(this);
	}

	private void showUserIcon() {
		User user = db.findFirst(User.class);
		if (user != null && user.getIcon1() != null) {
			BitmapUtils bitmapUtils = BitmapHelp.getBitmapUtils(this);
			// // 加载网络图片
			ImageView mHead1 = (ImageView) findViewById(R.id.base_id_mhead);
			ImageView mHead = (ImageView) findViewById(R.id.one);

			bitmapUtils.display(mHead, user.getIcon1());
			bitmapUtils.display(mHead1, user.getIcon1());

		}

	}

	@Override
	protected void inject() {
		// 忽略
	}

	@Override
	protected void onResume() {
		super.onResume();
		// 刷新未读数
		refreshMessage();
		showUserIcon();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		if (mTipExit) {
			if (mExit) {
				finish();
			} else {
				mExit = true;
				AppUtil.showToast(this, R.string.main_exit);
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						mExit = false;
					}
				}, 2000);
			}
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mMsgReceiver);
	}

	/**
	 * 
	 */
	@Override
	public void setContentView(int layoutResId) {
		View view = LayoutInflater.from(this).inflate(layoutResId, null);
		mFlRoot.addView(view);
	}

	/**
	 * 
	 */
	@Override
	public void setContentView(View view) {
		mFlRoot.addView(view);
	}

	/**
	 * 是否显示返回功能
	 * 
	 * @param enabled
	 */
	public void enableBack(boolean enabled) {
		if (enabled) {
			mRlBack.setVisibility(View.VISIBLE);
		} else {
			mRlBack.setVisibility(View.GONE);
		}
	}

	/**
	 * 是否显示分享功能
	 * 
	 * @param enabled
	 */
	public void enableShare(boolean enabled) {
		if (enabled) {
			mRLshare.setVisibility(View.VISIBLE);
		} else {
			mRLshare.setVisibility(View.GONE);
		}
	}

	/**
	 * 是否显示删除功能
	 * 
	 * @param enabled
	 */
	public void enableDel(boolean enabled) {
		if (enabled) {
			mRLdel.setVisibility(View.VISIBLE);
		} else {
			mRLdel.setVisibility(View.GONE);
		}
	}

	/**
	 * 是否显示发表功能
	 * 
	 * @param enabled
	 */
	public void enablePublish(boolean enabled) {
		if (enabled) {
			mRLpublish.setVisibility(View.VISIBLE);
		} else {
			mRLpublish.setVisibility(View.GONE);
		}
	}

	/**
	 * 是否显示系统消息
	 * 
	 * @param enabled
	 */
	public void enableMessage(boolean enabled) {
		if (enabled) {
			mRlMessage.setVisibility(View.VISIBLE);
			refreshMessage();
		} else {
			mRlMessage.setVisibility(View.GONE);
		}
	}

	/**
	 * 是否显示进度条（圈）
	 * 
	 * @param enabled
	 */
	public void enableProgress(boolean enabled) {
		if (enabled) {
			mPbProgress.setVisibility(View.VISIBLE);
			refreshMessage();
		} else {
			mPbProgress.setVisibility(View.GONE);
		}
	}

	public void enableSearch(boolean enabled) {
		if (enabled) {
			mRlSearch.setVisibility(View.VISIBLE);
		} else {
			mRlSearch.setVisibility(View.GONE);
		}
	}

	/**
	 * 是否有进度显示
	 */
	public boolean isProgress() {
		return mPbProgress.getVisibility() == View.VISIBLE;
	}

	/**
	 * 刷新未读数
	 */
	public void refreshMessage() {
		if (mRlMessage.getVisibility() == View.VISIBLE) {
			// MessageDao msgDao = new MessageDao(this);
			// int count = msgDao.getUnreadCount();
			// if (count > 0) {
			// mTxtCount.setVisibility(View.VISIBLE);
			// mTxtCount.setText(count + "");
			// } else {
			// mTxtCount.setVisibility(View.GONE);
			// }
		}
	}

	/**
	 * 设置标题
	 */
	public void setTopTitle(int resId) {
		mTxtTitle.setText(resId);
	}

	/**
	 * 设置标题
	 * 
	 * @param title
	 */
	public void setTopTitle(String title) {
		mTxtTitle.setText(title == null ? "" : title);
	}

	/***
	 * 是否显示标题栏
	 * 
	 * @param enabled
	 */
	public void enableShowHeadTop(boolean enabled) {
		if (enabled) {
			mHeadTop.setVisibility(View.VISIBLE);
		} else {
			mHeadTop.setVisibility(View.GONE);
		}
	}

	/**
	 * 是否显示刷新功能
	 * 
	 * @param enabled
	 */
	public void enableRefresh(boolean enabled) {
		if (enabled) {
			mRlRefresh.setVisibility(View.VISIBLE);
		} else {
			mRlRefresh.setVisibility(View.GONE);
		}
	}

	/**
	 * 是否显示左上角头像功能
	 * 
	 * @param head
	 */
	public void headRefresh(boolean head) {
		if (head) {
			mRlHead.setVisibility(View.VISIBLE);
		} else {
			mRlHead.setVisibility(View.GONE);
		}
	}

	/**
	 * 监听返回事件
	 * 
	 * @param listener
	 */
	public void setOnBackListener(OnBackListener listener) {
		this.mBackListener = listener;
	}

	/**
	 * 监听刷新事件
	 * 
	 * @param listener
	 */
	public void setOnRefreshListener(OnRefreshListener listener) {
		this.mRefreshListener = listener;
	}

	/**
	 * 监听分享事件
	 * 
	 * @param listener
	 */
	public void setOnShareListener(OnShareListener listener) {
		this.mShareListener = listener;
	}

	/**
	 * 监听分享事件
	 * 
	 * @param listener
	 */
	public void setOnDelListener(OnDelListener listener) {
		this.mDelListener = listener;
	}

	/**
	 * 监听分享事件
	 * 
	 * @param listener
	 */
	public void setOnPublishListener(OnPublishListener listener) {
		this.mPublishListener = listener;
	}

	public void setOnSearchListener(OnSearchListener listener) {
		this.mSearchListener = listener;
	}

	/**
	 * 安Back键是否提示退出
	 * 
	 * @param enabled
	 */
	public void enableTipExit(boolean enabled) {
		mTipExit = enabled;
	}

	/**
	 * 显示无网络对话框
	 */
	public void showNetAvaiableDialog() {
		if (!mNetAvaiableDialog.isShowing()) {
			mNetAvaiableDialog.show();
		}
	}

	/**
	 * 
	 * @author zhenshui.xia
	 * 
	 */
	private class MessageReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			if (AppCst.MSG_ACTION.equals(intent.getAction())) {
				refreshMessage();
			}
		}

	}

	public interface OnBackListener {
		public void onBack();
	}

	public interface OnRefreshListener {
		public void onRefresh();
	}

	public interface OnShareListener {
		public void onShare();
	}

	public interface OnDelListener {
		public void onDel();
	}

	public interface OnPublishListener {
		public void onPublish();
	}

	public interface OnSearchListener {
		public void onSearch();
	}

	@Override
	public void onClick(View v) {
	}
}
