package com.wosai.teach.activity;

import android.app.Application;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

import com.umeng.analytics.AnalyticsConfig;
import com.umeng.analytics.MobclickAgent;
import com.umeng.message.PushAgent;
import com.wosai.teach.oss.OssManager;
import com.wosai.teach.utils.AppUtil;
import com.wosai.teach.utils.DBControl;

public class AppApplication extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		// 初始化,数据库orm
		DBControl.getInstance().init(getApplicationContext());
		//
		OssManager.getInstance().init(getApplicationContext());
		AppUtil.getInstance().init(getApplicationContext());
		// 初始化友盟统计
		ApplicationInfo appInfo;
		Boolean debug = false, encrypt = false;
		try {
			appInfo = this.getPackageManager().getApplicationInfo(
					getPackageName(), PackageManager.GET_META_DATA);
			debug = appInfo.metaData.getBoolean("UMENG_DEBUG", false);
			encrypt = appInfo.metaData.getBoolean("UMENG_ENCRYPT", false);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		MobclickAgent.setDebugMode(debug);
		AnalyticsConfig.enableEncrypt(encrypt);
		MobclickAgent.updateOnlineConfig(getApplicationContext());
		// 初始化推送
		PushAgent mPushAgent = PushAgent.getInstance(getApplicationContext());
		mPushAgent.enable();
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		// 销毁
	}
}
