package com.wosai.teach.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import com.lidroid.xutils.ViewUtils;
import com.umeng.analytics.MobclickAgent;
import com.wosai.teach.oss.OssCallBack;
import com.wosai.teach.utils.DBControl;
import com.wosai.teach.utils.ScreenManager;

public abstract class BaseFragmentActivity extends FragmentActivity implements OssCallBack{
	private final static String DEFAULT_PAGE_NAME = "temporaryNameActivity";
	private Context mContext;
	private String mPageName = DEFAULT_PAGE_NAME;
	

	
	public static DBControl db;

	private void genmPageName() {
		if (this.mPageName.equals(DEFAULT_PAGE_NAME)) {
			this.mPageName = this.getClass().toString();
		}
		System.out.println("this.mPageName" + this.mPageName);
	};

	public void setmPageName(String mPageName) {
		this.mPageName = mPageName;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inject();
		db = DBControl.getInstance();
		mContext = this;
		
		this.genmPageName();
		MobclickAgent.openActivityDurationTrack(false);
		ScreenManager.getScreenManager().pushActivity(this);
	}
	
	@Override
	protected void onDestroy() {
		ScreenManager.getScreenManager().popActivity(this);
		super.onDestroy();
	}

	protected void inject() {
		ViewUtils.inject(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onPageStart(mPageName);
		MobclickAgent.onResume(mContext);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd(mPageName);
		MobclickAgent.onPause(mContext);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Hook();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	// /对于好多应用，会在程序中杀死 进程，这样会导致我们统计不到此时Activity结束的信息，
	// /对于这种情况需要调用 'MobclickAgent.onKillProcess( Context )'
	// /方法，保存一些页面调用的数据。正常的应用是不需要调用此方法的。
	private void Hook() {
//		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//		builder.setPositiveButton("退出应用",
//				new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int whichButton) {
						MobclickAgent.onKillProcess(mContext);

//						int pid = android.os.Process.myPid();
//						android.os.Process.killProcess(pid);
//					}
//				});
//		builder.setNeutralButton("后退一下", new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int whichButton) {
				finish();
//			}
//		});
//		builder.setNegativeButton("点错了", new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int whichButton) {
//			}
//		});
//		builder.show();
	}
	

}
