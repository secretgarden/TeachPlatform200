package com.wosai.teach.activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.wosai.teach.R;
import com.wosai.teach.adapter.CacheAdapter;
import com.wosai.teach.adapter.CacheAdapter.ViewHolder;
import com.wosai.teach.cst.C;
import com.wosai.teach.dialog.LoginOutDialog;
import com.wosai.teach.entity.Experiment;
import com.wosai.teach.listener.CheckSureListener;
import com.wosai.teach.utils.AppUtil;
import com.wosai.teach.utils.ListViewUtils;
import com.wosai.teach.utils.OtherUtils;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月2日 下午2:07:29
 * @desc : 清理缓存
 */
@ContentView(R.layout.activity_cache)
public class CacheActivity extends AppActivity {

	@ViewInject(R.id.cache_none)
	private LinearLayout cacheNone;

	@ViewInject(R.id.cache_data)
	private LinearLayout cacheData;

	@ViewInject(R.id.cache_list)
	private ListView cacheList;

	@ViewInject(R.id.cache_del)
	private LinearLayout cacheDel;

	@ViewInject(R.id.cache_all)
	private TextView cacheAll;

	@ViewInject(R.id.cache_cancel)
	private TextView cacheCancel;

	private static final int CAN_DEL_COLOR = 0xff3f4657;

	private static final int NOT_DEL_COLOR = 0xffcecfcf;

	private CacheAdapter adapter;

	private boolean checkAll = false;

	private List<Experiment> expList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTopTitle("清理缓存");
		enableBack(true);
		enableDel(true);
		initView();
		queryData();
		setOnDelListener(new OnDelListener() {
			@Override
			public void onDel() {
				showDel();
			}
		});
	}

	private void initView() {
		adapter = new CacheAdapter(this, new ArrayList<Experiment>());
		cacheList.setAdapter(adapter);
		cacheList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				ViewHolder vh = (ViewHolder) view.getTag();
				adapter.choose(vh);
				canDel(vh.choose);
			}
		});
	}

	/**
	 * 展示删除场景
	 */
	private void showDel() {
		if (OtherUtils.CheckNull(expList)) {
			toastShow("无需清理");
		} else if (cacheDel.getVisibility() == View.GONE) {
			// 出现底部删除
			cacheDel.setVisibility(View.VISIBLE);
			adapter.showChoose(true);
			adapter.chooseAll(false);
			cacheAll.setText("全选");
			canDel(false);
			refreshHeight(true);
		} else {
			cacheDel.setVisibility(View.GONE);
			adapter.showChoose(false);
			refreshHeight(false);
		}
	}

	/**
	 * 增加底部的高度
	 * 
	 * @param b
	 */
	private void refreshHeight(boolean b) {
		int w = View.MeasureSpec.makeMeasureSpec(0,
				View.MeasureSpec.UNSPECIFIED);
		int h = View.MeasureSpec.makeMeasureSpec(0,
				View.MeasureSpec.UNSPECIFIED);
		cacheDel.measure(w, h);
		int height = cacheDel.getMeasuredHeight();

		LinearLayout.LayoutParams lp = (android.widget.LinearLayout.LayoutParams) cacheList
				.getLayoutParams();
		if (b) {
			lp.setMargins(0, 0, 0, height);
		} else {
			lp.setMargins(0, 0, 0, 0);
		}
		cacheList.setLayoutParams(lp);
	}

	@OnClick(R.id.cache_all)
	private void checkAll(View v) {
		checkAll = !checkAll;
		if (checkAll) {
			adapter.chooseAll(true);
			cacheAll.setText("取消");
			canDel(true);
		} else {
			adapter.chooseAll(false);
			cacheAll.setText("全选");
			canDel(false);
		}
	}

	/**
	 * 是否可以删除
	 * 
	 * @param can
	 */
	private void canDel(boolean can) {
		if (can) {
			cacheCancel.setTextColor(CAN_DEL_COLOR);
		} else {
			if (adapter.checkNoChoose()) {
				/* 没有被选中的选项 */
				cacheCancel.setTextColor(NOT_DEL_COLOR);
			}
		}
	}

	@OnClick(R.id.cache_del)
	private void checkDel(View v) {
		if (cacheCancel.getCurrentTextColor() == NOT_DEL_COLOR) {
			toastShow("请选择需要删除的选项");
			return;
		}
		LoginOutDialog dialog = new LoginOutDialog(this);
		dialog.init(new CheckSureListener() {

			@Override
			public void onOk() {
				adapter.unInstallChoose();
			}
		}, "确认卸载", "确定卸载所有选中的选项");
		dialog.show();

	}

	/**
	 * 查询缓存数据
	 */
	private void queryData() {
		expList = db.findAll(Selector.from(Experiment.class));
		if (!OtherUtils.CheckNull(expList)) {
			Iterator<Experiment> it = expList.iterator();
			while (it.hasNext()) {
				Experiment exp = it.next();
				PackageInfo pi = AppUtil
						.getPackageInfo(exp.getApkPackageName());
				int ver = AppUtil.getVersionCode(pi);
				if (ver <= 0) {
					it.remove();
				}
			}
			if (!OtherUtils.CheckNull(expList)) {
				cacheNone.setVisibility(View.GONE);
				cacheData.setVisibility(View.VISIBLE);
				adapter.setList(expList);
				adapter.notifyDataSetChanged();
				// 全选,删除所有, 所以一次加载所有
				ListViewUtils.setListViewHeightBasedOnChildren(cacheList);
				// refreshHeight(false);
				return;
			}
		}
		cacheNone.setVisibility(View.VISIBLE);
		cacheData.setVisibility(View.GONE);
		cacheDel.setVisibility(View.GONE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == C.REQUEST_CODE_UNINSTALL) {
			adapter.chooseAll(false);
			queryData();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		canDel(false);
	}
}
