package com.wosai.teach.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.wosai.teach.R;
import com.wosai.teach.utils.AppUtil;

@ContentView(R.layout.activity_feedback)
public class FeedbackActivity extends AppActivity {

	@ViewInject(R.id.feedback_title)
	private EditText title;

	@ViewInject(R.id.feedback_context)
	private EditText context;

	@ViewInject(R.id.feedback_bt)
	private Button bt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTopTitle("意见反馈");
		setmPageName("意见反馈");
		enableBack(true);
		initView();
	}

	private void initView() {
	}

	@OnClick(R.id.feedback_bt)
	public void submit(View view) {
		AppUtil.showToast(this, "意见已提交，感谢参与");
	}
}
