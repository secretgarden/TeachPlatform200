package com.wosai.teach.activity;

import com.lidroid.xutils.view.annotation.ContentView;
import com.wosai.teach.R;
import com.wosai.teach.utils.AppUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TabHost.OnTabChangeListener;


@ContentView(R.layout.activity_homework_and_score)
public class HomeworkAndScoreActivity extends AppFragmentActivity {

	// 作业
	public static final String TAB_HOMEWORK = "HomeworkTab";

	// 成绩
	public static final String TAB_SCORE = "ScoreTab";

	public static HomeworkAndScoreActivity myAct;

	private static MyTabHost mTabhost;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTopTitle("作业/成绩");
		enableBack(true);
		myAct = this;

		mTabhost = (MyTabHost) myAct.findViewById(android.R.id.tabhost);
		mTabhost.setup();

		// 作业
		mTabhost.addTab(mTabhost
				.newTabSpec(TAB_HOMEWORK)
				.setIndicator(
						newTabIndicator(R.string.homework))
				.setContent(R.id.tab_homework));
		// 成绩
		mTabhost.addTab(mTabhost
				.newTabSpec(TAB_SCORE)
				.setIndicator(
						newTabIndicator(R.string.score))
				.setContent(R.id.tab_score));

		mTabhost.setOnTabChangedListener(null);
		
		lockMenu();
	}

	/**
	 * 
	 * @param resTxt
	 * @param resImg
	 * @return
	 */
	private View newTabIndicator(int resTxt) {
		View indicator = LayoutInflater.from(this).inflate(
				R.layout.h_s_tab_indicator, null);
		ImageView icon = (ImageView) indicator
				.findViewById(R.id.h_s_tab_indicator_id_icon);
		
		
		TextView text = (TextView) indicator
				.findViewById(R.id.h_s_tab_indicator_id_text);
		text.setText(resTxt);
		return indicator;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mTabhost = null;
	}

	/**
	 * 切换标签
	 * 
	 * @param tag
	 */
	public void setCurrentTab(String tag) {
		if (mTabhost != null) {
			mTabhost.setCurrentTabByTag(tag);
			if (HomeworkAndScoreActivity.TAB_HOMEWORK.equals(tag)) {
				((HomeworkAndScoreActivity) getApplicationContext()).setTopTitle("作业");
			} else if (HomeworkAndScoreActivity.TAB_SCORE.equals(tag)) {
				((HomeworkAndScoreActivity) getApplicationContext()).setTopTitle("成绩");
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			Intent it = new Intent();
			it.putExtra("expId", 0);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	

}