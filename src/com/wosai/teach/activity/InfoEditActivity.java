package com.wosai.teach.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.widget.EditText;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.wosai.teach.R;
import com.wosai.teach.cst.C;
import com.wosai.teach.dto.JsonResult;
import com.wosai.teach.entity.User;
import com.wosai.teach.utils.OtherUtils;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月6日 上午9:46:52
 * @desc : 修改信息
 */
@ContentView(R.layout.activity_info_edit)
public class InfoEditActivity extends AppActivity {

	@ViewInject(R.id.info_edit_et)
	private EditText et;

	private User user;

	/**
	 * 修改字段标识
	 */
	private int id;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setmPageName("个人信息的修改");
		Intent it = getIntent();
		setTopTitle(it.getStringExtra("title"));
		id = it.getIntExtra("id", 0);
		enableBack(true);
		enablePublish(true);
		initView();
	}

	/**
	 * 
	 */
	private void initView() {
		user = db.findFirst(User.class);
		switch (id) {
		case C.INFO_EDIT_EMAIL:
			et.setHint("请输入邮箱");
			et.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
			et.setText(user.getEmail());
			break;
		case C.INFO_EDIT_MOBILE:
			et.setHint("请输入手机");
			et.setInputType(InputType.TYPE_CLASS_PHONE);
			et.setFilters(new InputFilter[] { new InputFilter.LengthFilter(11) });
			et.setText(user.getMobile());
			break;
		case C.INFO_EDIT_MICROINFO:
			et.setHint("请输入微信");
			et.setFilters(new InputFilter[] { new InputFilter.LengthFilter(10) });
			et.setText(user.getMicroMsg());
			break;
		case C.INFO_EDIT_QQ:
			et.setHint("请输入QQ");
			et.setInputType(InputType.TYPE_CLASS_NUMBER);
			et.setFilters(new InputFilter[] { new InputFilter.LengthFilter(10) });
			et.setText(user.getQQ());
			break;
		default:
			break;
		}
		setOnPublishListener(new OnPublishListener() {

			@Override
			public void onPublish() {
				saveEdit();
			}
		});
	}

	/**
	 * 保存修改
	 */
	private void saveEdit() {
		String string = et.getText().toString();
		User userChange = new User();
		userChange.setUserId(user.getUserId());
		switch (id) {
		case C.INFO_EDIT_EMAIL:
			if (OtherUtils.isEmailValid(string)) {
				userChange.setEmail(string);
			} else if (OtherUtils.CheckNull(string)) {
				userChange.setEmail("");
			} else {
				toastShow("请输入正确的邮箱格式");
				return;
			}
			break;
		case C.INFO_EDIT_MOBILE:
			if (OtherUtils.isMobileNO(string)) {
				userChange.setMobile(string);
			} else if (OtherUtils.CheckNull(string)) {
				userChange.setMobile("");
			} else {
				toastShow("请输入正确的手机号格式");
				return;
			}
			break;
		case C.INFO_EDIT_MICROINFO:
			userChange.setMicroMsg(string);
			break;
		case C.INFO_EDIT_QQ:
			if ("10000".equals(string)) {
				toastShow("这可是官方扣扣！");
				return;
			} else if (OtherUtils.isQQ(string) && (string.length() > 4)) {
				userChange.setQQ(string);
			} else if (OtherUtils.CheckNull(string)) {
				userChange.setQQ("");
			} else {
				toastShow("请输入5-10位数字");
				return;
			}
			break;
		default:
			break;
		}
		String jsonStr = JSON.toJSONString(userChange);
		updateInfo(jsonStr);
	}

	/**
	 * 调用接口
	 */
	private void updateInfo(String content) {
		HttpUtils http = new HttpUtils();
		String url = C.HTTP_USER_INFO_UPDATE;
		url = url.replace("{userId}", user.getUserId() + "");
		RequestParams params = new RequestParams();
		params.addBodyParameter("content", content);
		http.send(HttpMethod.POST, url, params, new RequestCallBack<String>() {

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				if (OtherUtils.CheckNull(responseInfo.result)) {
					return;
				}
				System.out.println("||changeInformation||"
						+ responseInfo.result);
				JsonResult jr = JSON.parseObject(responseInfo.result,
						JsonResult.class);
				if (jr.getResult() == 0) {
					User user = JSON.parseObject(jr.getObject().toString(),
							User.class);
					if (user == null) {
						return;
					}
					db.deleteAll(Selector.from(User.class).where("userId", "=",
							user.getUserId()));
					db.save(user);
					finish();
				}
				toastShow(jr.getMessage());
			}

			@Override
			public void onFailure(HttpException error, String msg) {
				System.out.println("==================="
						+ error.fillInStackTrace());
			}

		});
	}
}
