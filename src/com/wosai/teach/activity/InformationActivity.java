package com.wosai.teach.activity;

import android.content.Intent;
//git.oschina.net/secretgarden/TeachPlatform200.gitimport android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.wosai.teach.R;
import com.wosai.teach.cst.C;
import com.wosai.teach.entity.User;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月4日 下午5:01:07
 * @desc : 展示修改信息
 */
@ContentView(R.layout.activity_information)
public class InformationActivity extends AppActivity {

	@ViewInject(R.id.information_code)
	private TextView ifCode;
	@ViewInject(R.id.information_username)
	private TextView ifUserName;
	@ViewInject(R.id.information_sex)
	private TextView ifSex;
	@ViewInject(R.id.information_class)
	private TextView ifClass;
	@ViewInject(R.id.information_email)
	private TextView ifEmail;
	@ViewInject(R.id.information_mobile)
	private TextView ifMobile;
	@ViewInject(R.id.information_microMsg)
	private TextView ifMicroMsg;
	@ViewInject(R.id.information_qq)
	private TextView ifQQ;
	@ViewInject(R.id.if_1)
	private LinearLayout if_1;
	@ViewInject(R.id.if_2)
	private LinearLayout if_2;
	@ViewInject(R.id.if_3)
	private LinearLayout if_3;
	@ViewInject(R.id.if_4)
	private LinearLayout if_4;
	@ViewInject(R.id.if_5)
	private LinearLayout if_5;
	@ViewInject(R.id.if_6)
	private LinearLayout if_6;
	@ViewInject(R.id.if_7)
	private LinearLayout if_7;
	@ViewInject(R.id.if_8)
	private LinearLayout if_8;

	private User user;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setmPageName("展示个人信息，可以进行个人信息的修改");
		setTopTitle(R.string.information);
		enableBack(true);
		initView();
	}

	private void initView() {
		refresh();
	}

	@OnClick(value = { R.id.if_5, R.id.if_6, R.id.if_7, R.id.if_8 })
	public void clickEdit(View v) {
		Intent it = new Intent(this, InfoEditActivity.class);
		switch (v.getId()) {
		case R.id.if_5:
			it.putExtra("title", "设置邮箱");
			it.putExtra("id", C.INFO_EDIT_EMAIL);
			break;
		case R.id.if_6:
			it.putExtra("title", "设置手机号");
			it.putExtra("id", C.INFO_EDIT_MOBILE);
			break;
		case R.id.if_7:
			it.putExtra("title", "设置微信号");
			it.putExtra("id", C.INFO_EDIT_MICROINFO);
			break;
		case R.id.if_8:
			it.putExtra("title", "设置QQ号");
			it.putExtra("id", C.INFO_EDIT_QQ);
			break;
		default:
			break;
		}
		startActivityForResult(it, C.REQUEST_CODE_INFO_EDIT);
	}

	private void refresh() {
		user = db.findFirst(User.class);
		ifCode.setText(user.getNickName());
		ifUserName.setText(user.getUserName());
		if (user.getSex().equals("female")) {
			ifSex.setText("女");
		} else {
			ifSex.setText("男");
		}
		ifClass.setText(user.getClassId() + "班");
		ifEmail.setText(user.getEmail());
		ifMobile.setText(user.getMobile());
		ifMicroMsg.setText(user.getMicroMsg());
		ifQQ.setText(user.getQQ());
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == C.REQUEST_CODE_INFO_EDIT) {
			refresh();
		}
	}
}
