package com.wosai.teach.activity;

import java.util.ArrayList;
import java.util.List;

import org.android.agoo.client.BaseRegistrar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.view.annotation.ContentView;
import com.umeng.update.UmengUpdateAgent;
import com.wosai.teach.R;
import com.wosai.teach.adapter.SimulationAdapter;
import com.wosai.teach.adapter.SimulationAdapter.ViewHolder;
import com.wosai.teach.cst.C;
import com.wosai.teach.entity.Experiment;
import com.wosai.teach.entity.GridInfo;
import com.wosai.teach.fragment.SimulationFragment;
import com.wosai.teach.listener.SimuOnItemClickListener;
import com.wosai.teach.service.DownLoadFactory;
import com.wosai.teach.service.DownLoadService;
import com.wosai.teach.utils.AppUtil;
import com.wosai.teach.view.RoundProgressBar;

@ContentView(R.layout.activity_main)
public class MainActivity extends AppFragmentActivity {

	// 教学仿真
	public static final String TAB_SIMULATION = "SimulationTab";

	// 微客
	public static final String TAB_MICROCLASS = "MicroClassTab";

	// 题库
	public static final String TAB_QUESTIONS = "QuestionsTab";

	public static MainActivity myAct;

	private static MyTabHost mTabhost;

	private SimuOnItemClickListener listener;

	private DownLoadFactory factory;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		myAct = this;
		headRefresh(true);
		UmengUpdateAgent.update(this);

		mTabhost = (MyTabHost) findViewById(android.R.id.tabhost);
		mTabhost.setup();
		
		// 实验
		mTabhost.addTab(mTabhost
				.newTabSpec(TAB_SIMULATION)
				.setIndicator(
						newTabIndicator(R.string.main_simulation,
								R.drawable.main_simulation))
				.setContent(R.id.tab_main_simulation));
		// 微客
		mTabhost.addTab(mTabhost
				.newTabSpec(TAB_MICROCLASS)
				.setIndicator(
						newTabIndicator(R.string.main_micro_class,
								R.drawable.main_micro_class))
				.setContent(R.id.tab_main_micro_class));
		// 题库
		mTabhost.addTab(mTabhost
				.newTabSpec(TAB_QUESTIONS)
				.setIndicator(
						newTabIndicator(R.string.main_questions,
								R.drawable.main_questions))
				.setContent(R.id.tab_questions));
		mTabhost.setOnTabChangedListener(null);

		String device_token = BaseRegistrar.getRegistrationId(this);
		System.out.println("device_token||" + device_token);
	}

	/**
	 * 
	 * @param resTxt
	 * @param resImg
	 * @return
	 */
	private View newTabIndicator(int resTxt, int resImg) {
		View indicator = LayoutInflater.from(this).inflate(
				R.layout.main_tab_indicator, null);
		ImageView icon = (ImageView) indicator
				.findViewById(R.id.main_tab_indicator_id_icon);
		icon.setImageDrawable(this.getResources().getDrawable(resImg));
		TextView text = (TextView) indicator
				.findViewById(R.id.main_tab_indicator_id_text);
		text.setText(resTxt);
		return indicator;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mTabhost = null;
	}

	/**
	 * 切换标签
	 * 
	 * @param tag
	 */
	public void setCurrentTab(String tag) {
		if (mTabhost != null) {
			mTabhost.setCurrentTabByTag(tag);
			if (MainActivity.TAB_MICROCLASS.equals(tag)) {
				setTopTitle("中策仿真");
			} else if (MainActivity.TAB_MICROCLASS.equals(tag)) {
				setTopTitle("微课堂");
			} else if (MainActivity.TAB_QUESTIONS.equals(tag)) {
				setTopTitle("题库");
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			AppUtil.AppOut(keyCode, event, this);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@SuppressLint("NewApi")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == C.REQUEST_CODE_OPEN_SCORE && (resultCode == C.RESULT_CODE_DOWNLOAD || resultCode == C.RESULT_CODE_UPDATE)) {
			// TODO 调用下载
			int id = data.getIntExtra("expId", 0);
			closeMenu();
			if(id == 0){
				return;
			}
			
			List<Fragment> fragList = getSupportFragmentManager().getFragments();
			SimulationFragment simulation = (SimulationFragment) fragList.get(0);
			simulation.downloadApk(id);
		
		}
	}
}
