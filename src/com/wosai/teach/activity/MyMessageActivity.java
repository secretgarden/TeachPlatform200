package com.wosai.teach.activity;

import java.util.ArrayList;
import java.util.List;

import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.wosai.teach.R;
import com.wosai.teach.adapter.MessageAdapter;
import com.wosai.teach.entity.Message;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

@ContentView(R.layout.activity_mymessage)
public class MyMessageActivity extends AppActivity {

	@ViewInject(R.id.messagelist)
	private ListView listView;

	private List<Message> message;
	private MessageAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setmPageName("我的信息");
		setTopTitle("我的信息");
		enableBack(true);
		listView.setOnItemClickListener(listener);

		message = new ArrayList<Message>();
		Message msg = new Message();
		message.add(msg);

		adapter = new MessageAdapter(getApplicationContext(), message);
		listView.setAdapter(adapter);
		load();

		refreshHeight();
	}

	private void load() {
		// String url = C.;
		// System.out.println(url);
		//
		// HttpUtils http = new HttpUtils();
		// http.send(HttpRequest.HttpMethod.GET, url,
		// new RequestCallBack<String>() {
		//
		// @Override
		// public void onSuccess(ResponseInfo<String> responseInfo) {
		// JsonResult result = OtherUtils
		// .genJsonResult(responseInfo);
		//
		// if (result != null && result.getResult() == 0) {
		//
		// JsonResult jr = JSON.parseObject(
		// responseInfo.result, JsonResult.class);
		// message = JSON.parseArray(jr.getObject()
		// .toString(), Message.class);
		// adapter = new MessageAdapter(getApplicationContext(),
		// message);
		// listView.setAdapter(adapter);
		//
		// } else {
		// }
		// }
		//
		// @Override
		// public void onFailure(HttpException error, String msg) {
		// System.out.println(msg);
		// Toast.makeText(getApplicationContext(), "连接失败，请检查网络",
		// Toast.LENGTH_SHORT).show();
		// }
		//
		// });
		// if(adapter!=null){
		// refreshHeight();
		// }

	}

	private OnItemClickListener listener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Toast.makeText(getApplicationContext(), "点击事件", Toast.LENGTH_SHORT)
					.show();
			// MicroClass microClass = (MicroClass)
			// parent.getAdapter().getItem(position);
			Intent it = new Intent(getApplicationContext(), MyMsgChatActivity.class);
			// it.putExtra("expId", microClass.getId());
			startActivity(it);
		}
	};

	private void refreshHeight() {
		// 获取ListView对应的Adapter
		int totalHeight = 0;
		for (int i = 0, len = adapter.getCount(); i < len; i++) { // listAdapter.getCount()返回数据项的数目
			View listItem = adapter.getView(i, null, listView);
			listItem.measure(0, 0); // 计算子项View 的宽高
			totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (adapter.getCount() - 1));
		// listView.getDividerHeight()获取子项间分隔符占用的高度
		// params.height最后得到整个ListView完整显示需要的高度
		listView.setLayoutParams(params);

	}

}
