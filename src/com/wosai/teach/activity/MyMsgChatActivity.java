package com.wosai.teach.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.wosai.teach.R;
import com.wosai.teach.adapter.ChatMsgViewAdapter;
import com.wosai.teach.entity.ChatMsg;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

@ContentView(R.layout.activity_mymsgchat)
public class MyMsgChatActivity extends AppActivity {

	@ViewInject(R.id.btn_send)
	private Button mBtnSend;

	@ViewInject(R.id.et_sendmessage)
	private EditText mEditTextContent;
	// 聊天内容的适配器
	private ChatMsgViewAdapter mAdapter;

	@ViewInject(R.id.listview)
	private ListView mListView;

	// 聊天的内容
	private List<ChatMsg> mDataArrays = new ArrayList<ChatMsg>();

	private String[] msgArray = new String[] { "Hi,欢迎你来到沃赛教育平台，如果你在学习过程中有任何建议或问题，可以发消息给我！" };
	private String[] dataArray = new String[] { "2015-09-02 15:00" };
	private final static int COUNT = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setmPageName("沃赛课堂");
		setTopTitle("沃赛课堂");
		enableBack(true);
		initData();
	}

	private void initData() {
		for (int i = 0; i < COUNT; i++) {
			ChatMsg entity = new ChatMsg();
			entity.setDate(dataArray[i]);
			if (i % 2 == 0) {
				entity.setName("沃赛教育");
				entity.setMsgType(true);
			} else {
				entity.setName("我");
				entity.setMsgType(false);
			}

			entity.setText(msgArray[i]);
			mDataArrays.add(entity);
		}
		mAdapter = new ChatMsgViewAdapter(this, mDataArrays);
		mListView.setAdapter(mAdapter);

	}

	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.btn_send:
			send();
			break;
		}
	}
	/**
	 * 发送消息
	 */
	private void send() {
		 String contString = mEditTextContent.getText().toString();  
	        if (contString.length() > 0)  
	        {  
	            ChatMsg entity = new ChatMsg();  
	            entity.setDate(getDate());  
	            entity.setName("");  
	            entity.setMsgType(false);  
	            entity.setText(contString);  
	            mDataArrays.add(entity);  
	            mAdapter.notifyDataSetChanged();  
	            mEditTextContent.setText("");  
	            mListView.setSelection(mListView.getCount() - 1);  
	        }  
		
	}
	/**
	 * 获取日期
	 * @return
	 */
	private String getDate() {
		Calendar c = Calendar.getInstance();  
        String year = String.valueOf(c.get(Calendar.YEAR));  
        String month = String.valueOf(c.get(Calendar.MONTH));  
        String day = String.valueOf(c.get(Calendar.DAY_OF_MONTH) + 1);  
        String hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));  
        String mins = String.valueOf(c.get(Calendar.MINUTE));  
        StringBuffer sbBuffer = new StringBuffer();  
        sbBuffer.append(year + "-" + month + "-" + day + " " + hour + ":" + mins);   
        return sbBuffer.toString();  
	}

}
