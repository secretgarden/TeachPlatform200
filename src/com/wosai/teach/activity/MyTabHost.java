package com.wosai.teach.activity;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.TabHost;

import com.wosai.teach.dialog.OnceDialog;
import com.wosai.teach.listener.CheckSureListener;

public class MyTabHost extends TabHost {
	public MyTabHost(Context context) {
		super(context);
	}

	public MyTabHost(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MyTabHost(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs);
	}

	@Override
	public void setOnTabChangedListener(OnTabChangeListener l) {
		super.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tag) {
				if (MainActivity.TAB_SIMULATION.equals(tag)) {
					MainActivity.myAct.setTopTitle("中策仿真");
				} else if (MainActivity.TAB_MICROCLASS.equals(tag)) {
					// MainActivity.myAct.setTopTitle("微课堂");
					setCurrentTabByTag(MainActivity.TAB_SIMULATION);
					alertUn();
				} else if (MainActivity.TAB_QUESTIONS.equals(tag)) {
					// MainActivity.myAct.setTopTitle("题库");
					setCurrentTabByTag(MainActivity.TAB_SIMULATION);
					alertUn();
				}
			}
		});
	}

	/**
	 * 提示未完成
	 */
	public void alertUn() {
		final OnceDialog dialog = new OnceDialog(this.getContext());
		dialog.init(new CheckSureListener() {
			@Override
			public void onOk() {

			}
		}, "提示", "该模块尚未开通，敬请期待");
		dialog.show();
		dialog.setCanceledOnTouchOutside(true);
		Handler hand = new Handler();
		hand.postDelayed(new Runnable() {

			@Override
			public void run() {
				dialog.cancel();
			}
		}, 1 * 1000);
	}

	@Override
	public void setCurrentTabByTag(String tag) {
		if (MainActivity.TAB_SIMULATION.equals(tag)
				|| MainActivity.TAB_MICROCLASS.equals(tag)
				|| MainActivity.TAB_QUESTIONS.equals(tag)) {
			// boolean toLogin = AppUtil.toLogin(MainActivity.myAct);
			// if (toLogin) {
			// return;
			// }
		}
		super.setCurrentTabByTag(tag);
	}

	@Override
	public void setCurrentTab(int index) {
		// TODO index == 2
		if (index == 1 || index == 3 || index == 2) {
			// boolean toLogin = AppUtil.toLogin(MainActivity.myAct);
			// if (toLogin) {
			// return;
			// }
		}
		super.setCurrentTab(index);
	}

}
