package com.wosai.teach.activity;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.widget.ListView;

import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.wosai.teach.R;
import com.wosai.teach.adapter.NewsAdapter;
import com.wosai.teach.entity.News;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月3日 下午3:00:39
 * @desc :
 */
@ContentView(R.layout.activity_news)
public class NewsActivity extends AppActivity {

	@ViewInject(R.id.news_list)
	private ListView mListView;

	private NewsAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setmPageName("我的消息");
		setTopTitle("我的消息");
		enableBack(true);
		initData();
	}

	private void initData() {
		List<News> newsList = new ArrayList<News>();
		News news3 = new News("温馨提示", "2015-08-22 5:43:10", "初九\n潜龙勿用", 0, "");
		News news1 = new News("温馨提示", "2015-09-02 10:25:30", "九月你好", 1, "李老师");
		News news2 = new News("温馨提示", "2015-09-03 15:15:20", "今天阅兵", 1, "张老师");
		newsList.add(news3);
		newsList.add(news1);
		newsList.add(news2);
		adapter = new NewsAdapter(this, newsList);
		mListView.setAdapter(adapter);
	}
}
