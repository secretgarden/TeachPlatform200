package com.wosai.teach.activity;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ListView;

import com.alibaba.fastjson.JSON;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.view.annotation.ContentView;
import com.wosai.teach.R;
import com.wosai.teach.adapter.RankAdapter;
import com.wosai.teach.cst.C;
import com.wosai.teach.dto.JsonResult;
import com.wosai.teach.entity.Ranking;
import com.wosai.teach.entity.User;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年6月18日 下午3:01:36
 * @desc : 详细排名
 */
@ContentView(R.layout.activity_rank_dtl)
public class RankDetailAction extends AppActivity {
	private int expId;
	private int pageSize = 10;
	private int curPage = 1;
	private String expName;
	private PullToRefreshListView rankList;
	private RankAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTopTitle("详细排名");
		enableBack(true);

		Intent it = getIntent();
		this.expId = it.getIntExtra("expId", 0);
		this.expName = it.getStringExtra("expName");
		System.out.println(this.expName);

		initView();
		loadSoftInfo();
	}

	public void initView() {
		rankList = (PullToRefreshListView) this
				.findViewById(R.id.rank_dtl_list);
		rankList.setMode(Mode.BOTH);
		rankList.setOnRefreshListener(new OnRefreshListener2<ListView>() {

			@Override
			public void onPullDownToRefresh(
					final PullToRefreshBase<ListView> refreshView) {
				System.out.println("下拉刷新" + refreshView.getClass());
				curPage = 1;
				loadSoftInfo();
				// 结束加载
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						refreshView.onRefreshComplete();
					}
				}, 1500);
			}

			@Override
			public void onPullUpToRefresh(
					final PullToRefreshBase<ListView> refreshView) {
				System.out.println("上拉加载" + refreshView.getClass());
				curPage++;
				loadSoftInfo();
				// 结束加载
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						refreshView.onRefreshComplete();
					}
				}, 1500);

			}
		});
	}

	/**
	 * @param rankList
	 * 
	 *            刷新显示排名
	 */
	private void refreshRankList(List<Ranking> rankDtoList) {
		if (adapter == null) {
			adapter = new RankAdapter(this, rankDtoList);
			rankList.setAdapter(adapter);
		} else if (curPage == 1) {
			adapter.setList(rankDtoList);
			adapter.notifyDataSetChanged();
		} else {
			adapter.addList(rankDtoList);
			adapter.notifyDataSetChanged();
		}
		rankList.onRefreshComplete();
	}

	private void loadSoftInfo() {
		String url = C.HTTP_URL_RANK_DTL_LIST;
		User user = db.findFirst(User.class);
		url = url.replace("{userId}", user.getUserId() + "");
		url = url.replace("{expId}", expId + "");
		url = url.replace("{pageSize}", this.pageSize + "");
		url = url.replace("{currentPage}", this.curPage + "");
		System.out.println(url);

		HttpUtils http = new HttpUtils();
		http.send(HttpRequest.HttpMethod.GET, url,
				new RequestCallBack<String>() {

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						JsonResult jr = JSON.parseObject(responseInfo.result,
								JsonResult.class);
						List<Ranking> list = null;
						if (jr.getResult() == 0) {
							list = JSON.parseArray(jr.getObject().toString(),
									Ranking.class);
						}
						refreshRankList(list);
					}

					@Override
					public void onFailure(HttpException error, String msg) {

					}

				});
	}

}
