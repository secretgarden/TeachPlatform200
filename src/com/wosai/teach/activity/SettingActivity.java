package com.wosai.teach.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.wosai.teach.R;
import com.wosai.teach.dialog.LoginOutDialog;
import com.wosai.teach.entity.AppUseInfo;
import com.wosai.teach.entity.User;
import com.wosai.teach.listener.CheckSureListener;
import com.wosai.teach.utils.AppUtil;

@ContentView(R.layout.activity_setting)
public class SettingActivity extends AppActivity {

	@ViewInject(R.id.set_password)
	private TextView password;

	@ViewInject(R.id.set_grade)
	private LinearLayout grade;

	@ViewInject(R.id.set_grade_1)
	private TextView txtGrade;

	@ViewInject(R.id.set_about)
	private TextView about;

	@ViewInject(R.id.set_out)
	private TextView setOut;

	private User user;

	private String[] stringGrade = { "七年级上", "七年级下", "八年级上", "八年级下", "九年级上",
			"九年级下" };
	private RadioOnClick radioOnClick = new RadioOnClick(0);
	private ListView areaRadioListView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTopTitle("设置");
		setmPageName("设置");
		enableBack(true);
		initView();

		user = db.findFirst(User.class);

		if (user != null) {
			setOut.setVisibility(View.VISIBLE);
		} else {
			setOut.setVisibility(View.GONE);
		}
		setOut.setOnClickListener(this);
	}

	private void initView() {
		PackageInfo packageInfo = AppUtil.getPackageInfo("com.wosai.teach");
		String passwordName = AppUtil.getVersionName(packageInfo);
		// String newName = update.getText() + "(" + passwordName + ")";
		// update.setText(newName);

	}

	@OnClick(R.id.set_password)
	public void showpassword(View view) {
		System.out.println("showpassword");
		Intent it = new Intent(this, ModifyActivity.class);
		startActivity(it);
	}

	@OnClick(R.id.set_grade)
	public void toScoreList(View view) {
		System.out.println("年级");
		AlertDialog ad = new AlertDialog.Builder(SettingActivity.this)
				.setTitle("选择年级")
				.setSingleChoiceItems(stringGrade, radioOnClick.getIndex(),
						radioOnClick).create();
		areaRadioListView = ad.getListView();
		ad.show();
	}

	@OnClick(R.id.set_about)
	public void toabout(View view) {
		System.out.println("关于沃赛");
		Intent it = new Intent(this, AboutActivity.class);
		startActivity(it);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.set_out: {
			checkLoginOut();
			break;
		}
		default:
			break;
		}
		super.onClick(v);
	}

	/**
	 * 是否退出登录
	 */
	private void checkLoginOut() {
		LoginOutDialog dialog = new LoginOutDialog(this);
		dialog.init(new CheckSureListener() {

			@Override
			public void onOk() {
				System.out.println("退出登录");
				AppUseInfo aui = db.getFirst(AppUseInfo.class);
				aui.setLogin(false);
				db.saveOrUpdate(aui);
				Intent it = new Intent(SettingActivity.this,
						LoginActivity.class);
				startActivity(it);
				finish();
			}
		}, "退出登录", "退出后不会删除任何历史数据,下次登录依然可以使用本帐号");
		dialog.show();
	}

	class RadioOnClick implements DialogInterface.OnClickListener {
		private int index;

		public RadioOnClick(int index) {
			this.index = index;
		}

		public void setIndex(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void onClick(DialogInterface dialog, int whichButton) {
			setIndex(whichButton);
			Toast.makeText(SettingActivity.this,
					"您已经选择了： " + index + ":" + stringGrade[index],
					Toast.LENGTH_SHORT).show();

			txtGrade.setText(stringGrade[index]);

			dialog.dismiss();
		}
	}
}
