package com.wosai.teach.activity;

import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.widget.TextView;

import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.wosai.teach.R;
import com.wosai.teach.utils.AppUtil;

@ContentView(R.layout.activity_default)
public class VersionActivity extends AppActivity {

	@ViewInject(R.id.default_tv)
	private TextView tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTopTitle("版本信息");
		setmPageName("版本信息");
		enableBack(true);
		initView();
	}

	private void initView() {
		PackageInfo packageInfo = AppUtil.getPackageInfo("com.wosai.teach");
		String versionName = AppUtil.getVersionName(packageInfo);
		StringBuffer sb = new StringBuffer();
		sb.append("\n");
		sb.append("沃赛教育Android" + versionName);
		sb.append("\n").append("\n").append("\n").append("\n");
		sb.append("@2015 wosai, all rights reserved");
		tv.setText(sb.toString());
	}
}
