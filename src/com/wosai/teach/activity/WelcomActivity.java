package com.wosai.teach.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.wosai.teach.R;
import com.wosai.teach.view.BannerView;

@ContentView(R.layout.activity_welcom)
public class WelcomActivity extends BaseActivity {

	@ViewInject(R.id.welcom_banner_view)
	private BannerView bannerView;

	private BitmapUtils bitmapUtils;

	private Button bt1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initView();
	}

	public void initView() {
		Integer[] layout = { R.layout.welcom_item_one, R.layout.welcom_item_two,R.layout.welcom_item_three };
		bannerView.initAttribute(bitmapUtils);
		bannerView.addViews(layout, false);

		View v2 = bannerView.getView(2);
		bt1 = (Button) v2.findViewById(R.id.first_bt1);
		bt1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
