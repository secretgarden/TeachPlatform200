package com.wosai.teach.adapter;

import java.util.List;

import android.app.Activity;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.wosai.teach.R;
import com.wosai.teach.cst.C;
import com.wosai.teach.dto.JsonResult;
import com.wosai.teach.entity.Honor;
import com.wosai.teach.entity.Medal;
import com.wosai.teach.entity.Ranking;
import com.wosai.teach.entity.User;
import com.wosai.teach.utils.BitmapHelp;
import com.wosai.teach.utils.DBControl;
import com.wosai.teach.utils.ListViewUtils;
import com.wosai.teach.utils.OtherUtils;
import com.wosai.teach.view.NoScrollListView;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年6月17日 上午9:45:18
 * @desc :
 */
public class AchieveAdapter extends BaseAdapter {
	private Activity mActivity;
	private LayoutInflater mInflater;
	private List<Honor> list;
	private ListView listView;
	private SparseIntArray sa = new SparseIntArray();
	private BitmapUtils bitmapUtils;
	private DBControl dbc;
	private int cur = 0;
	private boolean showFlag = false;

	public AchieveAdapter(Activity activity, List<Honor> list, DBControl dbc) {
		this.dbc = dbc;
		this.mActivity = activity;
		this.mInflater = LayoutInflater.from(activity);
		bitmapUtils = BitmapHelp.getBitmapUtils(activity);
		this.list = list;
	}

	public boolean isCur(int cur) {
		if (this.cur == cur) {
			return true;
		} else {
			return false;
		}
	}

	public void setListView(ListView listView) {
		this.listView = listView;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	public void setList(List<Honor> list) {
		this.list = list;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.achieve_list_item, null);
			holder = new ViewHolder();
			holder.mIcon = (ImageView) convertView
					.findViewById(R.id.achieve_icon);
			holder.mName = (TextView) convertView
					.findViewById(R.id.achieve_name);
			holder.mGold = (ImageView) convertView
					.findViewById(R.id.medal_gold);
			holder.mSilver = (ImageView) convertView
					.findViewById(R.id.medal_silver);
			holder.mList = (NoScrollListView) convertView
					.findViewById(R.id.rank_list);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Honor honor = list.get(position);
		bitmapUtils.display(holder.mIcon, honor.getExpIcon1());

		holder.mName.setText(honor.getExpName());
		holder.mGold.setImageResource(R.drawable.medal_gold1);
		holder.mSilver.setImageResource(R.drawable.medal_silver1);
		List<Medal> medalList = honor.getListMedalDto();
		if (!OtherUtils.CheckNull(medalList)) {
			for (Medal mdeal : medalList) {
				if (mdeal.getLevel() == 1 && mdeal.getHonRecActive() == 1) {
					holder.mGold.setImageResource(R.drawable.medal_gold2);
				} else if (mdeal.getLevel() == 0
						&& mdeal.getHonRecActive() == 1) {
					holder.mSilver.setImageResource(R.drawable.medal_silver2);
				}
			}
		}
		if (!showFlag) {
			// showRank(0);
		}
		return convertView;
	}

	/**
	 * 展示排名
	 */
	public void showRank(int position) {
		hiddenRank();
		cur = position;
		View view = listView.getChildAt(cur);
		if (view == null) {
			return;
		}
		showFlag = true;
		ViewHolder vh = (ViewHolder) view.getTag();
		vh.mList.setVisibility(View.VISIBLE);
		if (vh.mList.getAdapter() != null && cur < 0) {
			return;
		}

		Honor honor = list.get(cur);
		String url = C.HTTP_URL_RANK_LIST;
		User user = dbc.findFirst(User.class);
		url = url.replace("{userId}", user.getUserId() + "");
		url = url.replace("{expId}", honor.getExpId() + "");
		url = url.replace("{topX}", 3 + "");
		url = url.replace("{beforYAndMe}", 2 + "");
		url = url.replace("{afterZ}", 1 + "");
		System.out.println(url);

		HttpUtils http = new HttpUtils();
		http.send(HttpRequest.HttpMethod.GET, url,
				new RequestCallBack<String>() {

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						JsonResult jr = JSON.parseObject(responseInfo.result,
								JsonResult.class);
						List<Ranking> list = null;
						if (jr.getResult() == 0) {
							list = JSON.parseArray(jr.getObject().toString(),
									Ranking.class);
						}
						refreshRankList(list);
					}

					@Override
					public void onFailure(HttpException error, String msg) {

					}

				});
	}

	/**
	 * @param rankList
	 * 
	 *            刷新显示排名
	 */
	private void refreshRankList(List<Ranking> rankList) {
		if (OtherUtils.CheckNull(rankList)) {
			return;
		}
		View view = listView.getChildAt(cur);
		if (view == null) {
			return;
		}
		ViewHolder vh = (ViewHolder) view.getTag();
		vh.mList.setVisibility(View.VISIBLE);
		// if (vh.mList.getAdapter() == null) {
		RankAdapter rankAdapter = new RankAdapter(mActivity, rankList);
		vh.mList.setAdapter(rankAdapter);
		// }
		ListViewUtils.setListViewHeightBasedOnChildren(vh.mList);
		int rankHeight = vh.mList.getLayoutParams().height;
		ListViewUtils.setListViewHeightBasedOnChildren(listView);
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = params.height + rankHeight;
		listView.setLayoutParams(params);
	}

	/**
	 * 刷新高度
	 */
	public void refreshHeight() {
		ListViewUtils.setListViewHeightBasedOnChildren(listView);
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		System.out.println("|当前cur|" + sa.get(cur) + "|params.height|"
				+ params.height);
		params.height = params.height + sa.get(cur);
		listView.setLayoutParams(params);
	}

	/**
	 * 隐藏排名
	 */
	private void hiddenRank() {
		View view = listView.getChildAt(cur);
		if (view == null) {
			return;
		}
		ViewHolder vh = (ViewHolder) view.getTag();
		vh.mList.setVisibility(View.GONE);
	}

	/**
	 * @author : qiumy
	 * @e-mail : qiumy@wosaitech.com
	 * @date : 2015-4-2 上午10:35:43
	 * @desc :
	 */
	public class ViewHolder {
		// icon
		public ImageView mIcon;
		// 标题
		public TextView mName;

		public ImageView mGold;

		public ImageView mSilver;

		public NoScrollListView mList;
	}

}
