package com.wosai.teach.adapter;

import java.util.List;

import com.wosai.teach.R;
import com.wosai.teach.entity.ChatMsg;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ChatMsgViewAdapter extends BaseAdapter {

	// ListView视图的内容由IMsgViewType决定
	public static interface IMsgViewType {
		// 对方发来的信息
		int IMVT_COM_MSG = 0;
		// 自己发出的信息
		int IMVT_TO_MSG = 1;
	}

	private static final String TAG = ChatMsgViewAdapter.class.getSimpleName();
	private List<ChatMsg> data;
	private Context context;
	private LayoutInflater mInflater;

	public ChatMsgViewAdapter(Context context, List<ChatMsg> data) {
		this.context = context;
		this.data = data;
		mInflater = LayoutInflater.from(context);
	}

	// 获取ListView的项个数
	public int getCount() {
		return data.size();
	}

	// 获取项
	public Object getItem(int position) {
		return data.get(position);
	}

	// 获取项的ID
	public long getItemId(int position) {
		return position;
	}

	// 获取项的类型
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		ChatMsg entity = data.get(position);

		if (entity.getMsgType()) {
			return IMsgViewType.IMVT_COM_MSG;
		} else {
			return IMsgViewType.IMVT_TO_MSG;
		}

	}

	// 获取项的类型数
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	// 获取View
	public View getView(int position, View convertView, ViewGroup parent) {

		ChatMsg entity = data.get(position);
		boolean isComMsg = entity.getMsgType();

		ViewHolderMyMsgChat holder = null;
		if (convertView == null) {
			if (isComMsg) {
				// 如果是对方发来的消息，则显示的是左气泡
				convertView = mInflater.inflate(
						R.layout.chatting_item_msg_text_left, null);
			} else {
				// 如果是自己发出的消息，则显示的是右气泡
				convertView = mInflater.inflate(
						R.layout.chatting_item_msg_text_right, null);
			}

			holder = new ViewHolderMyMsgChat();
			holder.ivUserhead = (ImageView) convertView
					.findViewById(R.id.iv_userhead);
			holder.tvSendTime = (TextView) convertView
					.findViewById(R.id.tv_sendtime);
			holder.tvUserName = (TextView) convertView
					.findViewById(R.id.tv_username);
			holder.tvContent = (TextView) convertView
					.findViewById(R.id.tv_chatcontent);
			holder.isComMsg = isComMsg;

			convertView.setTag(holder);
		} else {
			holder = (ViewHolderMyMsgChat) convertView.getTag();
		}

		holder.ivUserhead.setImageResource(R.drawable.icon);
		holder.tvSendTime.setText(entity.getDate());
		holder.tvUserName.setText(entity.getName());
		holder.tvContent.setText(entity.getText());

		return convertView;
	}

	// 通过ViewHolder显示项的内容
	static class ViewHolderMyMsgChat {
		public ImageView ivUserhead;
		public TextView tvSendTime;
		public TextView tvUserName;
		public TextView tvContent;
		public boolean isComMsg = true;
	}

}