package com.wosai.teach.adapter;

import java.util.List;

import com.wosai.teach.R;
import com.wosai.teach.entity.DetailsComment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

class ViewHolder2 {
	/**
	 * 他人头像
	 */
	public ImageView otherIcon;
	/**
	 * 他人姓名
	 */
	public TextView otherName;
	/**
	 * 他人评论点赞
	 */
	public TextView otherCommentPraise;
	/**
	 * 他人评论
	 */
	public TextView otherComment;
	/**
	 * 他人评论时间
	 */
	public TextView otherCommentTime;
}

public class DetailsListAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<DetailsComment> list;

	public DetailsListAdapter(Context context, List<DetailsComment> list) {
		super();
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.list = list;
	}

	@Override
	public int getCount() {
//		return 5;
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void setList(List<DetailsComment> list) {
		this.list = list;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		System.out.println("details------"+position);
		ViewHolder2 holder = null;
		if (convertView == null) {
			holder = new ViewHolder2();
			convertView = mInflater.inflate(R.layout.frag_details_item, null);
			holder.otherIcon = (ImageView) convertView
					.findViewById(R.id.details_othericon);
			holder.otherName = (TextView) convertView
					.findViewById(R.id.details_othername);
			holder.otherCommentPraise = (TextView) convertView
					.findViewById(R.id.details_othercomment_praise);
			holder.otherComment = (TextView) convertView
					.findViewById(R.id.details_othercomment);
			holder.otherCommentTime = (TextView) convertView
					.findViewById(R.id.details_othercomment_time);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder2) convertView.getTag();
		}
		
		holder.otherIcon.setImageResource(R.drawable.load1);
		holder.otherName.setText("加载中");
		holder.otherCommentPraise.setText("加载中");
		holder.otherComment.setText("加载中");
		holder.otherCommentTime.setText("加载中");
		
		return convertView;
	}

}
