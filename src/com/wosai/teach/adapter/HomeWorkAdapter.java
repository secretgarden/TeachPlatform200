package com.wosai.teach.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.db.sqlite.Selector;
import com.wosai.teach.R;
import com.wosai.teach.cst.C;
import com.wosai.teach.entity.Experiment;
import com.wosai.teach.entity.HomeWork;
import com.wosai.teach.fragment.HomeworkFragment;
import com.wosai.teach.utils.BitmapHelp;
import com.wosai.teach.utils.DBControl;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-31 下午2:04:10
 * @desc :
 */
public class HomeWorkAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<HomeWork> list;
	private HomeworkFragment home;
	
	protected DBControl db;

	protected void init() {
		db = DBControl.getInstance();
	}

	public HomeWorkAdapter(Activity activity, List<HomeWork> list) {
		this.mInflater = LayoutInflater.from(activity);
		this.list = list;
	}

	public void setList(List<HomeWork> list) {
		this.list = list;
	}

	public void addList(List<HomeWork> list) {
		this.list.addAll(list);
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.home_work_item, null);
			holder = new ViewHolder();
			holder.hw_exp_name = (TextView) convertView
					.findViewById(R.id.hw_exp_name);
			holder.hw_dead_time = (TextView) convertView
					.findViewById(R.id.hw_dead_time);
			
			holder.hw_exp_img = (ImageView) convertView.findViewById(R.id.hw_exp_img);
			holder.hw_finish = (TextView) convertView.findViewById(R.id.hw_finish);
			holder.hw_score = (TextView) convertView.findViewById(R.id.hw_score);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		HomeWork hw = list.get(position);
		
		BitmapUtils bitmapUtils = new BitmapUtils(convertView.getContext() );
		bitmapUtils.display(holder.hw_exp_img, hw.getPicUrl());
		
		if(hw.getScore() != null){
			holder.hw_score.setVisibility(View.VISIBLE);
			holder.hw_score.setText("分数： "+hw.getScore()+"分");
			holder.hw_finish.setText("已完成");
			holder.hw_finish.setBackgroundResource(R.drawable.homework_finish);
		}else if(hw.getTimeout() == C.FINISH_TIMEOUT_1){
			holder.hw_score.setVisibility(View.INVISIBLE);
			holder.hw_finish.setText("超时未完成");
			holder.hw_finish.setBackgroundResource(R.drawable.homework_timeout);
		}else{
			holder.hw_score.setVisibility(View.INVISIBLE);
			holder.hw_finish.setText("开始实验");
			holder.hw_finish.setBackgroundResource(R.drawable.homework_unfinish);
		}
		
		
		holder.hw_exp_name.setText(hw.getExpName());
		
		holder.hw_dead_time.setText("截止时间：" + endTime(hw.getDeadtime()));
		
		holder.hw_finish.setTextSize(12);
		holder.hw_score.setTextSize(12);
		holder.hw_exp_name.setTextSize(14);
		holder.hw_dead_time.setTextSize(12);
		
		return convertView;
	}
	
	@SuppressLint("SimpleDateFormat")
	public String endTime(long date){
		
		SimpleDateFormat format=new SimpleDateFormat("MM-dd HH:mm");  
        Date d1=new Date(date);  
        String t1=format.format(d1);
		
		return t1;
		
	}

	/**
	 * @author : qiumy
	 * @e-mail : qiumy@wosaitech.com
	 * @date : 2015-4-2 上午10:35:43
	 * @desc :
	 */
	public class ViewHolder {
		public TextView hw_exp_name;

		public TextView hw_dead_time;
		
		public TextView hw_finish;
		
		public TextView hw_score;
		
		public ImageView hw_exp_img;
	}

}
