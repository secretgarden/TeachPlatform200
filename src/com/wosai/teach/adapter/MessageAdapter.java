package com.wosai.teach.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.wosai.teach.R;
import com.wosai.teach.entity.Message;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

class ViewHolderMsg {
	public ImageView msgImageView;
	public TextView msgName;
	public TextView msgTime;
	public TextView msgMsg;
	public TextView msgNum;
}

@SuppressLint("SimpleDateFormat")
public class MessageAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<Message> list;
	private Date dd = null;
	private String ss = null;

	public MessageAdapter(Context context, List<Message> list) {
		super();
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.list = list;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void setList(List<Message> list) {
		this.list = list;
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderMsg holder = null;
		if (convertView == null) {
			holder = new ViewHolderMsg();
			convertView = mInflater.inflate(R.layout.activity_list_mymessage,
					null);
			holder.msgImageView = (ImageView) convertView
					.findViewById(R.id.message_img);
			holder.msgName = (TextView) convertView
					.findViewById(R.id.message_name);
			holder.msgTime = (TextView) convertView
					.findViewById(R.id.message_time);
			holder.msgMsg = (TextView) convertView
					.findViewById(R.id.message_msg);
			holder.msgNum = (TextView) convertView
					.findViewById(R.id.message_num);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolderMsg) convertView.getTag();
		}

		Message msg = list.get(position);

		if (msg.getMsgTime() != 0) {
			dd = new Date(msg.getMsgTime());
			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
			ss = sdf.format(dd);
		} else {
			
		}

		holder.msgImageView.setImageResource(R.drawable.icon);
		holder.msgName.setText("沃赛课堂");
		holder.msgTime.setText("08-28");
		holder.msgMsg.setText("欢迎来到沃赛!");
		holder.msgNum.setText("1");

		return convertView;
	}

}
