package com.wosai.teach.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wosai.teach.R;
import com.wosai.teach.entity.MicroClass;

class ViewHolderMicroClass {
	public LinearLayout linerLayout;
	public ImageView imageView;
	public TextView recommendTitle;
	public TextView recommendPlaytimes;
	public TextView recommendTime;
}

public class MicroClassAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<MicroClass> list;
	private Date dd = null;
	private String ss = null;

	public MicroClassAdapter(Context context, List<MicroClass> list) {
		super();
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.list = list;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void setList(List<MicroClass> list) {
		this.list = list;
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		System.out.println("MicroClass------" + position);
		ViewHolderMicroClass holder = null;
		if (convertView == null) {
			holder = new ViewHolderMicroClass();
			convertView = mInflater.inflate(R.layout.frag_recommend_item, null);
			holder.linerLayout = (LinearLayout) convertView
					.findViewById(R.id.recommend);
			holder.imageView = (ImageView) convertView
					.findViewById(R.id.recommend_image);
			holder.recommendTitle = (TextView) convertView
					.findViewById(R.id.recommend_title);
			holder.recommendPlaytimes = (TextView) convertView
					.findViewById(R.id.recommend_playtimes);
			holder.recommendTime = (TextView) convertView
					.findViewById(R.id.recommend_time);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolderMicroClass) convertView.getTag();
		}

		MicroClass mic = list.get(position);

		holder.imageView.setImageResource(R.drawable.load1);
		holder.recommendTitle.setText("名称：" + mic.getName());
		holder.recommendPlaytimes.setText("播放次数：" + mic.getPlayTimes());
		
		if (mic.getCreateDate()!=0) {
			if (mic.getUpdateDate()!=0) {
				dd = new Date(mic.getUpdateDate());
				SimpleDateFormat sdf = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				ss = sdf.format(dd);
			} else {
				dd = new Date(mic.getCreateDate());
				SimpleDateFormat sdf = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				ss = sdf.format(dd);
			}

		}

		holder.recommendTime.setText("时间：" + ss);
		System.out.println("mic.getName()" + mic.getName());
		System.out.println("mic.getPlayTimes()" + mic.getPlayTimes());
		System.out.println("mic.getCreateDate()" + ss);

		return convertView;
	}
}