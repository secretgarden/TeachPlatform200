package com.wosai.teach.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wosai.teach.R;
import com.wosai.teach.cst.C;
import com.wosai.teach.entity.News;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-31 下午2:04:10
 * @desc :
 */
public class NewsAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<News> list;

	public NewsAdapter(Activity activity, List<News> list) {
		this.mInflater = LayoutInflater.from(activity);
		this.list = list;
	}

	public void setList(List<News> list) {
		this.list = list;
	}

	public void addList(List<News> list) {
		this.list.addAll(list);
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.news_item, parent, false);
			holder = new ViewHolder();
			holder.newsSignLL = (LinearLayout) convertView
					.findViewById(R.id.news_sign_ll);
			holder.newsLL = (LinearLayout) convertView
					.findViewById(R.id.news_ll);
			holder.newsTime = (TextView) convertView
					.findViewById(R.id.news_time);
			holder.newsSign = (TextView) convertView
					.findViewById(R.id.news_sign);
			holder.newsContent = (TextView) convertView
					.findViewById(R.id.news_content);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		News news = list.get(position);
		holder.newsTime.setText(news.getDate());
		if (news.getType() == C.NEWS_TYPE_SYS) {
			/* 白字蓝底无签名 */
			holder.newsLL.setBackgroundResource(R.drawable.corner_shape_blue2);
			holder.newsContent.setTextColor(0xffffffff);
			holder.newsSignLL.setVisibility(View.GONE);
		} else {
			/* 黑字白底有签名 */
			holder.newsLL.setBackgroundResource(R.drawable.corner_shape_white);
			holder.newsContent.setTextColor(0xff000000);
			holder.newsSignLL.setVisibility(View.VISIBLE);
			holder.newsSign.setText(news.getSign());
		}

		holder.newsContent.setText(news.getContent());
		return convertView;
	}

	/**
	 * @author : qiumy
	 * @e-mail : qiumy@wosaitech.com
	 * @date : 2015-4-2 上午10:35:43
	 * @desc :
	 */
	class ViewHolder {
		public TextView newsTime;

		public TextView newsSign;

		public TextView newsContent;

		public LinearLayout newsLL;

		public LinearLayout newsSignLL;
	}

}
