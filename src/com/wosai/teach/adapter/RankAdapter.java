package com.wosai.teach.adapter;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.BitmapUtils;
import com.wosai.teach.R;
import com.wosai.teach.entity.Ranking;
import com.wosai.teach.utils.BitmapHelp;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年6月17日 下午5:17:26
 * @desc :
 */
public class RankAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<Ranking> list;
	private BitmapUtils bitmapUtils;

	public RankAdapter(Activity activity, List<Ranking> list) {
		this.mInflater = LayoutInflater.from(activity);
		bitmapUtils = BitmapHelp.getBitmapUtils(activity);
		this.list = list;
	}

	public void setList(List<Ranking> list) {
		this.list = list;
	}

	public void addList(List<Ranking> list) {
		this.list.addAll(list);
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.rank_item, null);
			holder = new ViewHolder();
			holder.mSerial = (TextView) convertView
					.findViewById(R.id.rank_serial);
			holder.mIcon = (ImageView) convertView
					.findViewById(R.id.rank_user_icon);
			holder.mName = (TextView) convertView
					.findViewById(R.id.rank_user_name);
			holder.mCost = (TextView) convertView.findViewById(R.id.rank_cost);
			holder.mDate = (TextView) convertView.findViewById(R.id.rank_date);
			holder.mTime = (TextView) convertView.findViewById(R.id.rank_time);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Ranking rank = list.get(position);
		holder.mSerial.setText(rank.getRank() + "");
		bitmapUtils.display(holder.mIcon, rank.getIcon1());
		holder.mName.setText(rank.getNickName());
		holder.mCost.setText(rank.getTimeCost() + "秒");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd",
				Locale.getDefault());
		holder.mDate.setText(sdf1.format(rank.getEndTime()));
		SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm",
				Locale.getDefault());
		holder.mTime.setText(sdf2.format(rank.getEndTime()));
		return convertView;
	}

	/**
	 * @author : qiumy
	 * @e-mail : qiumy@wosaitech.com
	 * @date : 2015-4-2 上午10:35:43
	 * @desc :
	 */
	class ViewHolder {
		public TextView mSerial;
		public ImageView mIcon;
		public TextView mName;
		public TextView mCost;
		public TextView mDate;
		public TextView mTime;
	}

}
