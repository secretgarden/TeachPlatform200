package com.wosai.teach.adapter;

import java.util.List;

import com.wosai.teach.R;
import com.wosai.teach.entity.Recommend;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

class ViewHolder {
	public LinearLayout linerLayout;
	public ImageView imageView;
	public TextView recommendTitle;
	public TextView recommendPlaytimes;
	public TextView recommendTime;
}

public class RecommendListAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<Recommend> list;

	public RecommendListAdapter(Context context, List<Recommend> list) {
		super();
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.list = list;
	}

	@Override
	public int getCount() {
		System.out.println("list size" + list.size());
		// return 5;
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void setList(List<Recommend> list) {
		this.list = list;
	}

	@Override
	public boolean areAllItemsEnabled() {
		// all items are separator
		return true;
	}

	@Override
	public boolean isEnabled(int position) {
		// all items are separator
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		System.out.println("recommend------" + position);
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.frag_recommend_item, null);
			holder.linerLayout = (LinearLayout) convertView
					.findViewById(R.id.recommend);
			holder.imageView = (ImageView) convertView
					.findViewById(R.id.recommend_image);
			holder.recommendTitle = (TextView) convertView
					.findViewById(R.id.recommend_title);
			holder.recommendPlaytimes = (TextView) convertView
					.findViewById(R.id.recommend_playtimes);
			holder.recommendTime = (TextView) convertView
					.findViewById(R.id.recommend_time);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.imageView.setImageResource(R.drawable.load1);
		holder.recommendTitle.setText("加载中");
		holder.recommendPlaytimes.setText("加载中");
		holder.recommendTime.setText("加载中");

//		holder.linerLayout.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				System.out.println("点击事件");
//			}
//		});

		return convertView;
	}
}