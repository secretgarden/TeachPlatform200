package com.wosai.teach.adapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.db.sqlite.Selector;
import com.wosai.teach.R;
import com.wosai.teach.entity.ExpRecUser;
import com.wosai.teach.entity.Experiment;
import com.wosai.teach.utils.BitmapHelp;
import com.wosai.teach.utils.DBControl;
import com.wosai.teach.utils.OtherUtils;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-31 下午2:04:10
 * @desc :
 */
public class ScoreAdapter extends BaseAdapter {
	private LayoutInflater mInflater;

	private List<ExpRecUser> list;

	private BitmapUtils bitmapUtils;

	private DBControl dbc;
	
	private String strTime;

	public ScoreAdapter(Activity activity, List<ExpRecUser> list, DBControl dbc) {
		this.mInflater = LayoutInflater.from(activity);
		bitmapUtils = BitmapHelp.getBitmapUtils(activity);
		this.list = list;
		this.dbc = dbc;
	}

	public void setList(List<ExpRecUser> list) {
		this.list = list;
	}

	public void addList(List<ExpRecUser> list) {
		this.list.addAll(list);
	}

	public boolean listIsEmpty() {
		return OtherUtils.CheckNull(list);
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.exp_score_item, null);
			holder = new ViewHolder();
			holder.mIcon = (ImageView) convertView.findViewById(R.id.exp_icon);
			holder.name = (TextView) convertView
					.findViewById(R.id.exp_rec_name);
			holder.score = (TextView) convertView
					.findViewById(R.id.exp_rec_score);
			holder.time = (TextView) convertView
					.findViewById(R.id.exp_rec_time);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		ExpRecUser exp = list.get(position);
		holder.name.setText(exp.getExpName());
		holder.name.setTextSize(14);
		holder.score.setText(exp.getScore() + "分");
		holder.score.setTextSize(14);

//		strTime = getChatTime(exp.getEndTime());
		strTime = getTime(exp.getEndTime());
		
		holder.time.setText(strTime);

		Experiment expt = this.dbc.findFirst(Selector.from(Experiment.class)
				.where("expId", "=", exp.getExpId()));
		if (expt != null) {
			bitmapUtils.display(holder.mIcon, expt.getIcon1());
		}
		return convertView;
	}

	/**
	 * @author : qiumy
	 * @e-mail : qiumy@wosaitech.com
	 * @date : 2015-4-2 上午10:35:43
	 * @desc :
	 */
	class ViewHolder {
		// icon
		public ImageView mIcon;
		// 标题
		public TextView name;
		// 描述
		public TextView score;
		// 完成时间
		public TextView time;

	}

	public String getChatTime(long time) {
		String result = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd");
		Date today = new Date(System.currentTimeMillis());
		Date otherDay = new Date(time);
		int temp = Integer.parseInt(sdf.format(today))
				- Integer.parseInt(sdf.format(otherDay));
		switch (temp) {
		case 0:
			result = "今天 " + getHourAndMin(time);
			break;
		case 1:
			result = "昨天 " + getHourAndMin(time);
			break;
		case 2:
			result = "前天 " + getHourAndMin(time);
			break;
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
			result = getWeekOfDate(otherDay) + getHourAndMin(time);
			break;			
		default:
			result = getTime(time);
			break;
		}
		return result;
	}

	public String getHourAndMin(long time) {
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		return format.format(new Date(time));
	}

	public String getTime(long time) {
		SimpleDateFormat format = new SimpleDateFormat("MM-dd HH:mm");
		return format.format(new Date(time));
	}
	public static String getWeekOfDate(Date dt) {
        String[] weekDays = {"周日 ", "周一 ", "周二 ", "周三 ", "周四 ", "周五 ", "周六 "};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

}
