package com.wosai.teach.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.http.HttpHandler;
import com.wosai.teach.R;
import com.wosai.teach.entity.DownloadInfo;
import com.wosai.teach.entity.Experiment;
import com.wosai.teach.entity.GridInfo;
import com.wosai.teach.listener.SimuOnItemClickListener;
import com.wosai.teach.utils.AppUtil;
import com.wosai.teach.utils.BitmapHelp;
import com.wosai.teach.utils.DBControl;
import com.wosai.teach.utils.OtherUtils;
import com.wosai.teach.view.RoundProgressBar;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月1日 上午10:55:46
 * @desc : 实验列表适配器
 */
public class SimulationAdapter extends BaseAdapter {

	private DBControl db;

	private Activity activity;

	private BitmapUtils bitmapUtils;

	private List<GridInfo> objList;

	private LayoutInflater mInflater;

	private SimuOnItemClickListener listener;

	private Map<String, Boolean> startMap = new HashMap<String, Boolean>();

	public SimulationAdapter(Activity activity, List<GridInfo> objList,
			DBControl db, SimuOnItemClickListener listener) {
		this.mInflater = LayoutInflater.from(activity);
		bitmapUtils = BitmapHelp.getBitmapUtils(activity);
		bitmapUtils.configDefaultLoadingImage(R.drawable.load1);
		bitmapUtils.configDefaultLoadFailedImage(R.drawable.load1);
		this.activity = activity;
		this.objList = objList;
		this.listener = listener;
		this.db = db;
	}

	public void setObjList(List<GridInfo> objList) {
		this.objList = objList;
	}

	@Override
	public int getCount() {
		return objList == null ? 0 : objList.size();
	}

	@Override
	public Object getItem(int position) {
		return objList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.simulation_content_item,
					parent, false);
			holder = new ViewHolder();
			holder.mName = (TextView) convertView.findViewById(R.id.tv_name);
			holder.mCount = (TextView) convertView.findViewById(R.id.tv_count);
			holder.mImage1 = (ImageView) convertView
					.findViewById(R.id.iv_image1);
			holder.mImage2 = (ImageView) convertView
					.findViewById(R.id.iv_image2);
			holder.darkBg = (ImageView) convertView.findViewById(R.id.dark_bg);
			holder.pause = (ImageView) convertView.findViewById(R.id.pause);
			holder.rpBar = (RoundProgressBar) convertView
					.findViewById(R.id.pbar);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.init();
		GridInfo grid = objList.get(position);
		holder.mName.setText(grid.getName());
		holder.mCount.setText(grid.getOtherDesc());
		updateImageHeight(holder);
		if (OtherUtils.CheckNull(grid.getImage())) {
			holder.mImage1.setImageResource(R.drawable.load1);
		} else if (grid.getImage().startsWith("http")) {
			// 网络图片
			bitmapUtils.display(holder.mImage1, grid.getImage());
		} else {
			Drawable dr = activity.getResources().getDrawable(
					Integer.valueOf(grid.getImage()));
			holder.mImage1.setImageDrawable(dr);
		}
		holder.mImage2.setBackgroundResource(R.drawable.eye);
		checkDownStatus(holder, grid);
		return convertView;
	}

	/**
	 * 拉伸图片高度
	 * 
	 * @param holder
	 * @param dr
	 */
	private void updateImageHeight(ViewHolder holder) {
		WindowManager wm = (WindowManager) activity
				.getSystemService(Context.WINDOW_SERVICE);
		int width = wm.getDefaultDisplay().getWidth();
		width -= AppUtil.dip2px(activity, 20 + 10 * 2);// 减去marign部分
		// int height = width * 9 / 32;// 图片规格 9 : 16
		// int height = width / 2;// 图片规格 1 : 1
		int height = width * 2 / 5;// 图片规格 4 : 5
		holder.mImage1.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, height));
		holder.darkBg.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT, height));

		RelativeLayout.LayoutParams lp1 = getLayoutParams(60, width, height);
		holder.rpBar.setLayoutParams(lp1);
		RelativeLayout.LayoutParams lp2 = getLayoutParams(25, width, height);
		holder.pause.setLayoutParams(lp2);
	}

	private RelativeLayout.LayoutParams getLayoutParams(int dp, int width,
			int height) {
		int cellPx = AppUtil.dip2px(activity, dp);
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
				cellPx, cellPx);
		lp.setMargins(width / 4 - cellPx / 2, height / 2 - cellPx / 2, 0, 0);
		return lp;
	}

	/**
	 * 判断下载状态
	 */
	private void checkDownStatus(ViewHolder holder, GridInfo grid) {
		Boolean start = startMap.containsKey(grid.getRelId());
		start = start == null ? true : start;
		if (start) {
			startMap.put(grid.getRelId(), false);
		}

		Experiment exp = db.findFirst(Selector.from(Experiment.class).where(
				"expId", "=", grid.getRelId()));
		DownloadInfo downloadInfo = listener.refreshDownLoadInfo(exp);
		HttpHandler.State state = downloadInfo.getState();
		switch (state) {
		case STARTED:
		case WAITING:
		case SUCCESS:
			break;
		case LOADING:
			if (start) {
				// 下载中退出,状态虽然是下载中,但是实际上和取消逻辑相同
				listener.stopDownload(exp.getExpId() + "");
			} else {
				holder.showBar();
				break;
			}
		case CANCELLED:
			holder.showPause(downloadInfo.getPercent());
			break;
		case FAILURE:
			break;
		default:
			break;
		}
	}

	public class ViewHolder {
		public TextView mName;
		public TextView mCount;
		public ImageView mImage1;
		public ImageView mImage2;
		public ImageView darkBg;
		public ImageView pause;
		public RoundProgressBar rpBar;

		public void init() {
			this.initVisible();
		}

		public void initVisible() {
			this.rpBar.setVisibility(View.GONE);
			this.darkBg.setVisibility(View.GONE);
			this.pause.setVisibility(View.GONE);
		}

		public void showPause(int result) {
			this.initVisible();
			this.rpBar.setTextIsDisplayable(false);
			this.rpBar.setProgress(result);
			this.darkBg.setVisibility(View.VISIBLE);
			this.pause.setVisibility(View.VISIBLE);
			this.rpBar.setVisibility(View.VISIBLE);
		}

		public void showBar() {
			this.initVisible();
			this.rpBar.setTextIsDisplayable(true);
			this.darkBg.setVisibility(View.VISIBLE);
			this.rpBar.setVisibility(View.VISIBLE);
		}
	}
}
