package com.wosai.teach.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.bitmap.BitmapCommonUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.core.BitmapSize;
import com.wosai.teach.R;
import com.wosai.teach.entity.GridInfo;
import com.wosai.teach.utils.AppUtil;
import com.wosai.teach.utils.BitmapHelp;
import com.wosai.teach.utils.OtherUtils;

public class SimulationContentAdapter extends BaseAdapter {

	private Activity activity;

	private BitmapUtils bitmapUtils;

	private List<GridInfo> objList;

	private LayoutInflater mInflater;

	public SimulationContentAdapter(Activity activity, List<GridInfo> objList) {
		this.mInflater = LayoutInflater.from(activity);
		bitmapUtils = BitmapHelp.getBitmapUtils(activity);
		bitmapUtils.configDefaultLoadingImage(R.drawable.load1);
		bitmapUtils.configDefaultLoadFailedImage(R.drawable.load1);
		this.activity = activity;
		this.objList = objList;
	}

	public void setObjList(List<GridInfo> objList) {
		this.objList = objList;
	}

	@Override
	public int getCount() {
		return objList == null ? 0 : objList.size();
	}

	@Override
	public Object getItem(int position) {
		return objList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.simulation_content_item,
					null);
			holder = new ViewHolder();
			holder.mName = (TextView) convertView.findViewById(R.id.tv_name);
			holder.mCount = (TextView) convertView.findViewById(R.id.tv_count);
			holder.mImage1 = (ImageView) convertView
					.findViewById(R.id.iv_image1);
			holder.mImage2 = (ImageView) convertView
					.findViewById(R.id.iv_image2);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		GridInfo grid = objList.get(position);
		holder.mName.setText(grid.getName());
		holder.mCount.setText(grid.getOtherDesc());

		updateImageHeight(holder);
		if (OtherUtils.CheckNull(grid.getImage())) {
			holder.mImage1.setImageResource(R.drawable.load1);
		} else if (grid.getImage().startsWith("http")) {
			// 网络图片
			BitmapDisplayConfig config = new BitmapDisplayConfig();
			BitmapSize size = config.getBitmapMaxSize();
			config.setBitmapMaxSize(BitmapCommonUtils.optimizeMaxSizeByView(
					holder.mImage1, size.getWidth(), size.getHeight()));
			Bitmap bitmap = bitmapUtils.getBitmapFromMemCache(grid.getImage(),
					config);
			if (bitmap != null) {
				System.out.println("getView||" + grid.getName() + "||有缓存");
				holder.mImage1.setImageBitmap(bitmap);
			} else {
				System.out.println("getView||" + grid.getName() + "||无缓存");
				bitmapUtils.display(holder.mImage1, grid.getImage());
			}
		} else {
			Drawable dr = activity.getResources().getDrawable(
					Integer.valueOf(grid.getImage()));
			holder.mImage1.setImageDrawable(dr);
		}
		holder.mImage2.setBackgroundResource(R.drawable.eye);
		holder.mImage1.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					final ImageView cur = (ImageView) v;
					changeLight(cur, -80);
					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							changeLight((ImageView) cur, 0);
						}
					}, 1000);
					break;
				case MotionEvent.ACTION_MOVE:
					break;
				case MotionEvent.ACTION_UP:
					break;
				default:
					break;
				}
				return false;
			}

			/**
			 * 滤镜变灰
			 * 
			 * @param imageview
			 * @param brightness
			 */
			private void changeLight(ImageView imageview, int brightness) {
				ColorMatrix matrix = new ColorMatrix();
				matrix.set(new float[] { 1, 0, 0, 0, brightness, 0, 1, 0, 0,
						brightness, 0, 0, 1, 0, brightness, 0, 0, 0, 1, 0 });
				imageview.setColorFilter(new ColorMatrixColorFilter(matrix));
			}
		});
		return convertView;
	}

	/**
	 * 拉伸图片高度
	 * 
	 * @param holder
	 * @param dr
	 */
	private void updateImageHeight(ViewHolder holder) {
		WindowManager wm = (WindowManager) activity
				.getSystemService(Context.WINDOW_SERVICE);
		int width = wm.getDefaultDisplay().getWidth();
		width -= AppUtil.dip2px(activity, 20);// 减去marign部分
		int height = width * 9 / 32;// 图片规格 9 : 16
		holder.mImage1.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, height));
	}

	public class ViewHolder {
		public TextView mName;
		public TextView mCount;
		public ImageView mImage1;
		public ImageView mImage2;
	}
}
