package com.wosai.teach.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wosai.teach.R;
import com.wosai.teach.entity.GridInfo;

public class SimulationTitleAdapter extends BaseAdapter {

	private List<GridInfo> objList;

	private LayoutInflater mInflater;

	public SimulationTitleAdapter(Activity activity, List<GridInfo> objList) {
		this.mInflater = LayoutInflater.from(activity);
		this.objList = objList;
	}

	public void setObjList(List<GridInfo> objList) {
		this.objList = objList;
	}

	@Override
	public int getCount() {
		return objList == null ? 0 : objList.size();
	}

	@Override
	public Object getItem(int position) {
		return objList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.simulation_title_item,
					null);
			holder = new ViewHolder();
			holder.mName = (TextView) convertView.findViewById(R.id.name);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		GridInfo grid = objList.get(position);
		holder.mName.setText(grid.getName());

		return convertView;
	}

	public class ViewHolder {
		public TextView mName;
	}
}
