package com.wosai.teach.cst;

public class AppCst extends DataBaseCst {
	public static final String MSG_ACTION = "com.android.inmotiontec.iparker.ACTION_MSG";

	public final static String HTTP_URL = "http://121.40.195.52:8080/teach_platform/service";
	// public final static String HTTP_URL =
	// "http://192.168.3.15:8080/teach/service";

	public final static String OSS_URL = "http://teach-platform.oss-cn-hangzhou.aliyuncs.com/";

	/* 头像名称 */
	public static final String IMAGE_FILE_NAME = "head.jpg";

	/**
	 * 相册
	 */
	public static final int PHOTO_REQUEST_CAMERA = 1;// 拍照

	public static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择

	public static final int PHOTO_REQUEST_CUT = 3;// 结果

	public static final int INFO_EDIT_EMAIL = 6001;// 邮箱

	public static final int INFO_EDIT_MOBILE = 6002;// 手机

	public static final int INFO_EDIT_MICROINFO = 6003;// 微信

	public static final int INFO_EDIT_QQ = 6004;// QQ
	
	/** 超时	 */
	public static final int FINISH_TIMEOUT_1 = 1;
	/** 未超时	 */
	public static final int FINISH_TIMEOUT_0 = 0;

}
