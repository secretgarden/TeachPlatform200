package com.wosai.teach.cst;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年7月31日 下午4:43:37
 * @desc :枚举类型
 */
public class EnumCst {
	/**
	 * 相册
	 */
	public static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择

	public static final int PHOTO_REQUEST_CUT = 3;// 结果

	public static final int REQUEST_CODE_INSTALL = 1001;

	public static final int REQUEST_CODE_UPDATE = 1002;

	public static final int REQUEST_CODE_UNINSTALL = 1003;

	public static final int REQUEST_CODE_OPEN = 1004;

	public static final int REQUEST_CODE_WELCOM = 1005;

	public final static int REQUEST_CODE_INFO_EDIT = 1006;

	public final static int REQUEST_CODE_EXP_DETAIL = 3001;

	/**
	 * 打开软件
	 */
	public final static int REQUEST_CODE_OPEN_SOFT = 3002;
	
	public final static int REQUEST_CODE_OPEN_SCORE = 3003;
	
	public final static int RESULT_CODE_DOWNLOAD = 4001;
	
	public final static int RESULT_CODE_UPDATE = 4002;

	public final static int NEWS_TYPE_SYS = 0;

	public final static int NEWS_TYPE_TEACHER = 1;
}
