package com.wosai.teach.cst;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年7月31日 下午4:44:27
 * @desc :接口常量
 */
public class UrlCst extends EnumCst {

//	public final static String HTTP_URL = "http://121.40.195.52:8080/teach_platform/service";
	public final static String HTTP_URL = "http://120.55.144.43/teach_platform/service";

	/**
	 * 用户登录
	 */
	public final static String HTTP_URL_LOGIN = HTTP_URL
			+ "/appLogin/{loginName}/{password}/{imei}/{imsi}/{appVer}/{osType}/{osVer}/{manufacture}/{phoneModel}/{rom}/{ram}/{sd}/{freeRom}/{freeRam}/{freeSd}/{screenX}/{screenY}/{network}";

	/**
	 * 修改用户资料
	 */
	public final static String HTTP_USER_INFO_UPDATE = HTTP_URL
			+ "/user/info/update/{userId}";
	
	/**
	 * 修改密码
	 */
	public final static String HTTP_USER_PASSWD_MODIFY = HTTP_URL
			+ "/user/password/update/{userId}";
	
	/**
	 * 获取大厅页面图片URL
	 */
	public final static String HTTP_URL_PIC_LOGIN_HALL = HTTP_URL
			+ "/pic/loginHall/{userId}/{pageSize}/{currentPage}";
	
	/**
	 *  获取实验清单
	 */
	public final static String HTTP_URL_EXPERIMENT_LIST = HTTP_URL
			+ "/homepageExp";

	/**
	 * 根据实验类型获取实验清单
	 */
	public final static String HTTP_URL_EXPERIMENT_LIST_BY_TYPE = HTTP_URL
			+ "/entity/experiment/type/{type}";

	/**
	 * 获取实验类型
	 */
	public final static String HTTP_URL_EXPERIMENT_TYPE = HTTP_URL
			+ "/homepage/expType";

	/**
	 *  获取实验详情
	 */
	public final static String HTTP_URL_EXP_DETAIL = HTTP_URL
			+ "/exp/detail/{expId}/{userId}";

	/**
	 *  获取大厅实验列表信息
	 */
	public final static String HTTP_URL_WHO_USED = HTTP_URL
			+ "/expRec/whoUsed/{expId}/{userId}/{pageSize}/{currentPage}";

	/**
	 *  获取本人实验记录(成绩)
	 */
	public final static String HTTP_URL_EXPERIMENT_REC_LIST = HTTP_URL
//			+ "/expRec/myRec/{userId}/{pageSize}/{curPage}";
	+ "/user/myHomework/score/{userId}/{pageSize}/{currentPage}";

	/**
	 *  获取个人勋章
	 */
	public final static String HTTP_URL_HONOR_LIST = HTTP_URL
			+ "/user/honor/{userId}";

	/**
	 *  获取排名概要
	 */
	public final static String HTTP_URL_RANK_LIST = HTTP_URL
			+ "/user/honor/ranking/overview/{userId}/{expId}/{topX}/{beforYAndMe}/{afterZ}";

	/**
	 *  获取详细排名
	 */
	public final static String HTTP_URL_RANK_DTL_LIST = HTTP_URL
			+ "/user/honor/ranking/detailed/{userId}/{expId}/{pageSize}/{currentPage}";

	/**
	 *  获取作业清单
	 */
	public final static String HTTP_URL_HOME_LIST = HTTP_URL
//			+ "/homework/classmates/{userId}/{pageSize}/{currentPage}";
	+ "/user/myHomework/{userId}/{pageSize}/{currentPage}";
	
	/**
	 * 获取微课堂分类
	 */
	public final static String HTTP_URL_HOMEPAGE_MICROCLASSTYPE = HTTP_URL
			+ "/entity/microcourseType";

	/**
	 * 微课堂
	 */
	public final static String HTTP_URL_MICROCLASS = HTTP_URL
			+ "/entity/microcourse";
}