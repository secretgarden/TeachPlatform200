package com.wosai.teach.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年7月31日 下午4:23:08
 * @desc :
 */
public class BaseDownDialog extends Dialog {
	protected Context mContext;

	public BaseDownDialog(Context context) {
		super(context);
		this.mContext = context;
	}

	public BaseDownDialog(Context context, int theme) {
		super(context, theme);
		this.mContext = context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void show() {
		super.show();

		Window window = getWindow();
		// 获取对话框当前的参数值
		WindowManager.LayoutParams params = window.getAttributes();
		// 高度
		params.height = android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
		// 宽度设置为屏幕的0.8
		DisplayMetrics mDm = mContext.getResources().getDisplayMetrics();
		params.width = (int) (mDm.widthPixels * 1);
		window.setAttributes(params);
		window.setGravity(Gravity.BOTTOM);
	}

}
