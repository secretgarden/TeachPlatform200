package com.wosai.teach.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.wosai.teach.R;
import com.wosai.teach.listener.CheckSureListener;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月1日 下午6:56:53
 * @desc :
 */
public class CheckSureDialog extends BaseDialog implements
		android.view.View.OnClickListener {

	private String title;

	private String text;

	private CheckSureListener list;

	public CheckSureDialog(Context context) {
		super(context, R.style.CheckSureDialog);

	}

	public CheckSureDialog(Context context, int theme) {
		super(context, theme);
	}

	public void init(CheckSureListener list, String title, String text) {
		this.list = list;
		this.text = text;
		this.title = title;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View view = getLayoutInflater().inflate(R.layout.check_sure_dialog,
				null);
		this.setContentView(view);
		TextView title = (TextView) findViewById(R.id.check_sure_title);
		title.setText(this.title);
		TextView text = (TextView) findViewById(R.id.check_sure_text);
		text.setText(this.text);
		findViewById(R.id.new_cancel).setOnClickListener(this);
		findViewById(R.id.new_ok).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.new_cancel:
			break;
		case R.id.new_ok:
			this.list.onOk();
			break;
		default:
			break;
		}
		cancel();
	}
}
