package com.wosai.teach.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.wosai.teach.R;
import com.wosai.teach.listener.ChooseHeadListener;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月1日 下午6:56:53
 * @desc :
 */
public class ChooseHeadDialog extends BaseDownDialog implements
		android.view.View.OnClickListener {

	private ChooseHeadListener list;

	public ChooseHeadDialog(Context context) {
		super(context, R.style.ChooseHeadDialog);
	}

	public ChooseHeadDialog(Context context, int theme) {
		super(context, theme);
	}

	public void init(ChooseHeadListener list) {
		this.list = list;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View view = getLayoutInflater().inflate(R.layout.choose_head_dialog,
				null);
		this.setContentView(view);
		TextView nivate = (TextView) findViewById(R.id.choose_nivate);
		TextView photo = (TextView) findViewById(R.id.choose_photo);
		TextView cancel = (TextView) findViewById(R.id.choose_cancel);
		nivate.setText("选择本地图片");
		photo.setText("拍照");
		cancel.setText("取消");
		nivate.setOnClickListener(this);
		photo.setOnClickListener(this);
		cancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.choose_cancel:
			break;
		case R.id.choose_nivate:
			list.onNative();
			break;
		case R.id.choose_photo:
			list.onPhoto();
			break;
		default:
			break;
		}
		cancel();
	}
}
