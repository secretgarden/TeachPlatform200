package com.wosai.teach.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.wosai.teach.R;
import com.wosai.teach.listener.CheckSureListener;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月1日 下午6:56:53
 * @desc :
 */
public class LoginOutDialog extends BaseDownDialog implements
		android.view.View.OnClickListener {

	private CheckSureListener list;

	private String sure;

	private String tip;

	public LoginOutDialog(Context context) {
		super(context, R.style.ChooseHeadDialog);
	}

	public LoginOutDialog(Context context, int theme) {
		super(context, theme);
	}

	public void init(CheckSureListener list, String sure, String tip) {
		this.list = list;
		this.sure = sure;
		this.tip = tip;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View view = getLayoutInflater()
				.inflate(R.layout.login_out_dialog, null);
		this.setContentView(view);
		TextView tip = (TextView) findViewById(R.id.login_out_tip);
		TextView sure = (TextView) findViewById(R.id.login_out_sure);
		TextView cancel = (TextView) findViewById(R.id.login_out_cancel);
		tip.setText(this.tip);
		sure.setText(this.sure);
		cancel.setText("取消");
		sure.setOnClickListener(this);
		cancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.login_out_cancel:
			break;
		case R.id.login_out_sure:
			list.onOk();
			break;
		default:
			break;
		}
		cancel();
	}
}
