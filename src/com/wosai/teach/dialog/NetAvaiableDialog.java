package com.wosai.teach.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.wosai.teach.R;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-5-13 上午9:32:04
 * @desc : 清除已下载应用对话框
 */
public class NetAvaiableDialog extends BaseDialog implements
		android.view.View.OnClickListener {

	public NetAvaiableDialog(Context context) {
		super(context);

	}

	public NetAvaiableDialog(Context context, int theme) {
		super(context, theme);

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View view = getLayoutInflater().inflate(R.layout.net_avaiable_dialog,
				null);
		this.setContentView(view);

		findViewById(R.id.net_avaiable_id_ok).setOnClickListener(this);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

	}

	@Override
	protected void onStart() {
		super.onStart();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.net_avaiable_id_ok:
			cancel();
			break;
		default:
			break;
		}
	}
}
