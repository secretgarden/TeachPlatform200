package com.wosai.teach.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wosai.teach.R;

public class NewProgressDialog extends Dialog {

	private ProgressBar pb;

	public NewProgressDialog(Context context, String strMessage) {
		this(context, R.style.CustomProgressDialog, strMessage);
	}

	public NewProgressDialog(Context context, int theme, String strMessage) {
		super(context, theme);
		this.setContentView(R.layout.new_progress_dialog);
		this.getWindow().getAttributes().gravity = Gravity.CENTER;
		pb = (ProgressBar) this.findViewById(R.id.loadingImageView);
		this.setCancelable(false);
		TextView tvMsg = (TextView) this.findViewById(R.id.id_tv_loadingmsg);
		if (tvMsg != null) {
			tvMsg.setText(strMessage);
		}
	}

	/**
	 * 隐藏进度
	 */
	public void hiddenProgressBar() {
		pb.setVisibility(View.GONE);
	}

	/**
	 * 开始
	 * 
	 * @param progressdialog
	 * @param context
	 * @param strMessage
	 * @param canBack
	 */
	public static NewProgressDialog showProgress(Context context,
			String strMessage, boolean canBack) {
		NewProgressDialog progressdialog = new NewProgressDialog(context,
				strMessage);
		progressdialog.setCancelable(canBack);
		progressdialog.show();
		return progressdialog;
	}

	/**
	 * 关闭
	 * 
	 * @param progressdialog
	 */
	public static void close(NewProgressDialog progressdialog) {
		progressdialog.cancel();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {

		if (!hasFocus) {
			// dismiss();
		}
	}
}
