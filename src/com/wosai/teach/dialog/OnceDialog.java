package com.wosai.teach.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.wosai.teach.R;
import com.wosai.teach.listener.CheckSureListener;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月1日 下午6:56:53
 * @desc :
 */
public class OnceDialog extends BaseDialog implements
		android.view.View.OnClickListener {

	private String title;

	private String text;

	private CheckSureListener list;

	public OnceDialog(Context context) {
		super(context, R.style.CheckSureDialog);

	}

	public OnceDialog(Context context, int theme) {
		super(context, theme);
	}

	public void init(CheckSureListener list, String title, String text) {
		this.list = list;
		this.text = text;
		this.title = title;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View view = getLayoutInflater().inflate(R.layout.once_dialog, null);
		this.setContentView(view);
		TextView title = (TextView) findViewById(R.id.check_sure_title);
		title.setText(this.title);
		TextView text = (TextView) findViewById(R.id.check_sure_text);
		text.setText(this.text);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		default:
			break;
		}
		cancel();
	}
}
