package com.wosai.teach.dto;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-31 下午2:34:02
 * @desc :
 */
public class JsonResult {

	/**
	 * 0,成功，1失败
	 */
	private int result;

	private int totalRec;// 不考虑分页条件的情况下符合查询条件总的记录条数

	private int curRec; // 本次结果中携带的符合分页条件的记录条数

	private String message;

	private Object object;

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public void setTotalRec(int totalRec) {
		this.totalRec = totalRec;
	}

	public int getTotalRec() {
		return totalRec;
	}

	public void setCurRec(int curRec) {
		this.curRec = curRec;
	}

	public int getCurRec() {
		return curRec;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
}
