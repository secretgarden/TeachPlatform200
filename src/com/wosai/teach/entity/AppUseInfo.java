package com.wosai.teach.entity;

import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Table;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年7月31日 下午5:37:22
 * @desc :使用信息表
 */
@Table(name = "appUseInfo")
public class AppUseInfo extends EntityBase {
	/**
	 * 是否第一次使用app
	 */
	@Column(column = "isFirstUse")
	private boolean isFirstUse;

	/**
	 * 登录名
	 */
	@Column(column = "loginName")
	private String loginName;

	/**
	 * 密码
	 */
	@Column(column = "loginPasswd")
	private String loginPasswd;

	/**
	 * 
	 */
	@Column(column = "userId")
	private Integer userId;

	/**
	 * 是否登录
	 */
	@Column(column = "login")
	private boolean login;

	public boolean isFirstUse() {
		return isFirstUse;
	}

	public void setFirstUse(boolean isFirstUse) {
		this.isFirstUse = isFirstUse;
	}

	public boolean isLogin() {
		return login;
	}

	public void setLogin(boolean login) {
		this.login = login;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginPasswd() {
		return loginPasswd;
	}

	public void setLoginPasswd(String loginPasswd) {
		this.loginPasswd = loginPasswd;
	}
}
