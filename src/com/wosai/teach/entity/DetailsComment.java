package com.wosai.teach.entity;

import java.io.Serializable;

import android.widget.ImageView;
import android.widget.TextView;

public class DetailsComment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 他人头像
	 */
	private ImageView otherIcon;
	/**
	 * 他人姓名
	 */
	private TextView otherName;
	/**
	 * 他人评论点赞
	 */
	private TextView otherCommentPraise;
	/**
	 * 他人评论
	 */
	private TextView otherComment;
	/**
	 * 他人评论时间
	 */
	private TextView otherCommentTime;

	public ImageView getDetailsOtherIcon() {
		return otherIcon;
	}

	public void setDetailsOtherIcon(ImageView detailsOtherIcon) {
		this.otherIcon = detailsOtherIcon;
	}

	public TextView getDetailsOtherName() {
		return otherName;
	}

	public void setDetailsOtherName(TextView detailsOtherName) {
		this.otherName = detailsOtherName;
	}

	public TextView getDetailsOtherCommentPraise() {
		return otherCommentPraise;
	}

	public void setDetailsOtherCommentPraise(TextView detailsOtherCommentPraise) {
		this.otherCommentPraise = detailsOtherCommentPraise;
	}

	public TextView getDetailsOtherComment() {
		return otherComment;
	}

	public void setDetailsOtherComment(TextView detailsOtherComment) {
		this.otherComment = detailsOtherComment;
	}

	public TextView getDetailsOtherCommentTime() {
		return otherCommentTime;
	}

	public void setDetailsOtherCommentTime(TextView detailsOtherCommentTime) {
		this.otherCommentTime = detailsOtherCommentTime;
	}

}
