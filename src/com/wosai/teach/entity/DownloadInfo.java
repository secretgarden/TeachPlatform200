package com.wosai.teach.entity;

import com.lidroid.xutils.db.annotation.Transient;
import com.lidroid.xutils.http.HttpHandler;

import java.io.File;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年8月5日 下午3:56:00
 * @desc : 下载信息
 */
public class DownloadInfo extends EntityBase {

	public DownloadInfo() {
	}

	@Transient
	private HttpHandler<File> handler;

	private HttpHandler.State state;

	private String downloadUrl;

	private String fileName;

	private String fileSavePath;

	private String relId;

	private long progress;

	private long fileLength;

	private boolean autoResume;

	private boolean autoRename;

	public HttpHandler<File> getHandler() {
		return handler;
	}

	public void setHandler(HttpHandler<File> handler) {
		this.handler = handler;
	}

	public HttpHandler.State getState() {
		return state;
	}

	public void setState(HttpHandler.State state) {
		this.state = state;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileSavePath() {
		return fileSavePath;
	}

	public void setFileSavePath(String fileSavePath) {
		this.fileSavePath = fileSavePath;
	}

	public String getRelId() {
		return relId;
	}

	public void setRelId(String relId) {
		this.relId = relId;
	}

	public long getProgress() {
		return progress;
	}

	public void setProgress(long progress) {
		this.progress = progress;
	}

	public int getPercent() {
		if (fileLength <= 0) {
			return 0;
		}
		return (int) (progress * 100 / fileLength);
	}

	public long getFileLength() {
		return fileLength;
	}

	public void setFileLength(long fileLength) {
		this.fileLength = fileLength;
	}

	public boolean isAutoResume() {
		return autoResume;
	}

	public void setAutoResume(boolean autoResume) {
		this.autoResume = autoResume;
	}

	public boolean isAutoRename() {
		return autoRename;
	}

	public void setAutoRename(boolean autoRename) {
		this.autoRename = autoRename;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof DownloadInfo))
			return false;

		DownloadInfo that = (DownloadInfo) o;
		if (getId() != that.getId()) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return (int) (getId() ^ (getId() >>> 32));
	}
}
