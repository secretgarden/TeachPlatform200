package com.wosai.teach.entity;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;

@Table(name = "ExpType")
public class ExpType {
	@Id
	// 如果主键没有命名名为id或_id的时，需要为主键添加此注解
	@NoAutoIncrement
	// int,long类型的id默认自增，不想使用自增时添加此注解
	private int id;

	private String createDate;

	private String name;

	private String remark;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
