package com.wosai.teach.entity;

import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Table;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年8月4日 下午1:33:19
 * @desc :grid信息
 */
@Table(name = "gridInfo")
public class GridInfo extends EntityBase {

	@Column(column = "className")
	private String className;

	@Column(column = "sequence")
	private int sequence;

	@Column(column = "image")
	private String image;

	@Column(column = "name")
	private String name;

	@Column(column = "relId")
	private String relId;

	@Column(column = "type")
	private String type;

	@Column(column = "date")
	private long date;

	/**
	 * 从上到下,从左到右
	 */
	@Column(column = "otherImage")
	private String otherImage;

	/**
	 * 从上到下,从左到右
	 */
	@Column(column = "otherDesc")
	private String otherDesc;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRelId() {
		return relId;
	}

	public void setRelId(String relId) {
		this.relId = relId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOtherImage() {
		return otherImage;
	}

	public void setOtherImage(String otherImage) {
		this.otherImage = otherImage;
	}

	public String getOtherDesc() {
		return otherDesc;
	}

	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

}
