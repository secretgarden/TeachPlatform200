package com.wosai.teach.entity;

import java.io.Serializable;

public class HomeWork extends EntityBase implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer expId;//实验ID

	private String expName;//实验名字

	private long deadtime;//截止时间
	
	private String picUrl;//实验图片
	
	private Integer score;//分数
	
	private Integer timeout;//超时

	public Integer getExpId() {
		return expId;
	}

	public void setExpId(Integer expId) {
		this.expId = expId;
	}

	public String getExpName() {
		return expName;
	}

	public void setExpName(String expName) {
		this.expName = expName;
	}

	public long getDeadtime() {
		return deadtime;
	}

	public void setDeadtime(long deadtime) {
		this.deadtime = deadtime;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

}