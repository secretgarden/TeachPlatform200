package com.wosai.teach.entity;

import java.util.List;

public class Honor extends EntityBase{

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Integer getExpId() {
		return expId;
	}

	public void setExpId(Integer expId) {
		this.expId = expId;
	}

	public String getExpName() {
		return expName;
	}

	public void setExpName(String expName) {
		this.expName = expName;
	}

	public String getExpIcon1() {
		return expIcon1;
	}

	public void setExpIcon1(String expIcon1) {
		this.expIcon1 = expIcon1;
	}

	public List<Medal> getListMedalDto() {
		return listMedalDto;
	}

	public void setListMedalDto(List<Medal> listMedalDto) {
		this.listMedalDto = listMedalDto;
	}

	public Honor() {
		super();
	}

	public Honor(Integer expId, String expName, String expIcon1) {
		super();
		this.expId = expId;
		this.expName = expName;
		this.expIcon1 = expIcon1;
	}

	public Honor(Integer userId, String nickName, Integer expId,
			String expName, String expIcon1) {
		super();
		this.userId = userId;
		this.nickName = nickName;
		this.expId = expId;
		this.expName = expName;
		this.expIcon1 = expIcon1;
	}

	// 以下字段来源于user表
	private Integer userId;// 用户的ID
	private String nickName;// 昵称

	// 以下字段来源于exp表
	private Integer expId;// 实验ID
	private String expName;
	private String expIcon1;// 实验的ICON

	// 该实验下的勋章列表。每个实验可能有多个MedalDTO。
	private List<Medal> listMedalDto;
}