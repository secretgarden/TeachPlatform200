package com.wosai.teach.entity;

import java.util.Date;

public class Medal extends EntityBase {

	public Integer getHonorRecID() {
		return honorRecID;
	}

	public void setHonorRecID(Integer honorRecID) {
		this.honorRecID = honorRecID;
	}

	public Integer getExpRecID() {
		return expRecID;
	}

	public void setExpRecID(Integer expRecID) {
		this.expRecID = expRecID;
	}

	public Integer getHonorId() {
		return honorId;
	}

	public void setHonorId(Integer honorId) {
		this.honorId = honorId;
	}

	public Integer getHonRecActive() {
		return honRecActive;
	}

	public void setHonRecActive(Integer honRecActive) {
		this.honRecActive = honRecActive;
	}

	public Date getTmHonRecCreate() {
		return tmHonRecCreate;
	}

	public void setTmHonRecCreate(Date tmHonRecCreate) {
		this.tmHonRecCreate = tmHonRecCreate;
	}

	public Date getTmHonRecUpdate() {
		return tmHonRecUpdate;
	}

	public void setTmHonRecUpdate(Date tmHonRecUpdate) {
		this.tmHonRecUpdate = tmHonRecUpdate;
	}

	public Integer getHonPicName() {
		return honPicName;
	}

	public void setHonPicName(Integer honPicName) {
		this.honPicName = honPicName;
	}

	public String getHonURL() {
		return honURL;
	}

	public void setHonURL(String honURL) {
		this.honURL = honURL;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getHonorName() {
		return honorName;
	}

	public void setHonorName(String honorName) {
		this.honorName = honorName;
	}

	public String getApplause() {
		return applause;
	}

	public void setApplause(String applause) {
		this.applause = applause;
	}

	public String getPromote() {
		return promote;
	}

	public void setPromote(String promote) {
		this.promote = promote;
	}

	public String getHonInfo() {
		return honInfo;
	}

	public void setHonInfo(String honInfo) {
		this.honInfo = honInfo;
	}

	// 以下字段来源于honorRec表
	private Integer honorRecID;// 荣誉记录编号
	private Integer expRecID;// 对应实验记录编号（如果荣誉获得和实验记录无关则有可能是空值！
	private Integer honorId;//
	private Integer honRecActive;// 用户是否被授予了该勋章。0：未授予勋章；1：已授予勋章；
	private Date tmHonRecCreate;// 创建荣誉记录的时间。当honRecActive=0时，此值为NULL；
	private Date tmHonRecUpdate;// 更新荣誉记录的时间。此值一般都为NULL。

	// 以下字段来源于honor表
	private Integer honPicName;// 图片名称
	private String honURL;// 保存勋章图片的URL地址
	private Integer level;// 荣誉授予等级
	private String honorName;// 显示给用户看的荣誉称号的官方名称
	private String applause;// 恭喜用户获得本勋章的话语。
	private String promote; // 推荐、鼓励用户挑战下一个难度等级的话语
	private String honInfo; // 描述信息，一般是用户获取勋章的条件描述。
}
