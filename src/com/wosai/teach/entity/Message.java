package com.wosai.teach.entity;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;

@Table(name = "Message")
public class Message {
	@Id
	// 如果主键没有命名名为id或_id的时，需要为主键添加此注解
	@NoAutoIncrement
	// int,long类型的id默认自增，不想使用自增时添加此注解
	private int id;
	private String msgImageView;
	private String msgName;
	private long msgTime;
	private String msgMsg;
	private int msgNum;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMsgImageView() {
		return msgImageView;
	}
	public void setMsgImageView(String msgImageView) {
		this.msgImageView = msgImageView;
	}
	public String getMsgName() {
		return msgName;
	}
	public void setMsgName(String msgName) {
		this.msgName = msgName;
	}
	public long getMsgTime() {
		return msgTime;
	}
	public void setMsgTime(long msgTime) {
		this.msgTime = msgTime;
	}
	public String getMsgMsg() {
		return msgMsg;
	}
	public void setMsgMsg(String msgMsg) {
		this.msgMsg = msgMsg;
	}
	public int getMsgNum() {
		return msgNum;
	}
	public void setMsgNum(int msgNum) {
		this.msgNum = msgNum;
	}
}
