package com.wosai.teach.entity;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;

@Table(name = "MicroClass")
public class MicroClass {
	@Id
	// 如果主键没有命名名为id或_id的时，需要为主键添加此注解
	@NoAutoIncrement
	// int,long类型的id默认自增，不想使用自增时添加此注解
	private int id;
	private long createDate;
	private long updateDate;
	private String name;
	private int badNum;
	private int goodNum;
	private String introduction;
	private int microcourseTypeId;
	private String playTimes;
	private String videoUrl;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getCreateDate() {
		return createDate;
	}

	public void setCreateDate(long createDate) {
		this.createDate = createDate;
	}

	public long getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(long updateDate) {
		this.updateDate = updateDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBadNum() {
		return badNum;
	}

	public void setBadNum(int badNum) {
		this.badNum = badNum;
	}

	public int getGoodNum() {
		return goodNum;
	}

	public void setGoodNum(int goodNum) {
		this.goodNum = goodNum;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public int getMicrocourseTypeId() {
		return microcourseTypeId;
	}

	public void setMicrocourseTypeId(int microcourseTypeId) {
		this.microcourseTypeId = microcourseTypeId;
	}

	public String getPlayTimes() {
		return playTimes;
	}

	public void setPlayTimes(String playTimes) {
		this.playTimes = playTimes;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}
}
