package com.wosai.teach.entity;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月3日 下午3:06:18
 * @desc : 消息
 */
public class News extends EntityBase {

	private String title;

	private String date;

	private String sign;

	private String content;

	/**
	 * NEWS_TYPE_SYS,官方,NEWS_TYPE_TEACHER,校方
	 */
	private Integer type;

	public News() {

	}

	public News(String title, String date, String content, Integer type,
			String sign) {
		this.title = title;
		this.date = date;
		this.type = type;
		this.sign = sign;
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
