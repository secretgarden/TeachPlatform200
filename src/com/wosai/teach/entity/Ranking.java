package com.wosai.teach.entity;

import java.io.Serializable;
import java.util.Date;

public class Ranking extends EntityBase implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Integer getTimeCost() {
		return timeCost;
	}

	public void setTimeCost(Integer timeCost) {
		this.timeCost = timeCost;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getExpID() {
		return expID;
	}

	public void setExpID(Integer expID) {
		this.expID = expID;
	}

	public String getExpName() {
		return expName;
	}

	public void setExpName(String expName) {
		this.expName = expName;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getIcon1() {
		return icon1;
	}

	public void setIcon1(String icon1) {
		this.icon1 = icon1;
	}

	// 以下字段来源于ExperimentRec
	private Integer timeCost;
	private Date endTime;

	// 以下字段来源于exp表
	private Integer expID;
	private String expName;

	// 以下字段来源于user表
	private Integer userID;
	private String nickName;// 昵称
	private String sex;// 性别
	private String icon1;// 用户头像的URL

	// 以下字段为动态生成的排名
	private Integer rank;
}
