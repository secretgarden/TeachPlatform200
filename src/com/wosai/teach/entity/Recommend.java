package com.wosai.teach.entity;

import java.io.Serializable;

import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Table;

@Table(name = "Recommend")
public class Recommend implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(column = "id")
	private int id;

	@Column(column = "createDate")
	private long createDate;

	@Column(column = "updateDate")
	private long updateDate;
	/**
	 * 相关推荐标题
	 */
	@Column(column = "name")
	private String name;

	@Column(column = "microcourseTypeId")
	private int microcourseTypeId;
	/**
	 * 相关推荐观看次数
	 */
	@Column(column = "playTimes")
	private String playTimes;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getCreateDate() {
		return createDate;
	}
	public void setCreateDate(long createDate) {
		this.createDate = createDate;
	}
	public long getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(long l) {
		this.updateDate = l;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMicrocourseTypeId() {
		return microcourseTypeId;
	}
	public void setMicrocourseTypeId(int microcourseTypeId) {
		this.microcourseTypeId = microcourseTypeId;
	}
	public String getPlayTimes() {
		return playTimes;
	}
	public void setPlayTimes(String playTimes) {
		this.playTimes = playTimes;
	}

}
