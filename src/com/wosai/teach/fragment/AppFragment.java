package com.wosai.teach.fragment;

import com.wosai.teach.utils.DBControl;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.Toast;

public class AppFragment extends Fragment {

	private final static String DEFAULT_PAGE_NAME = "appFragment";

	private Context mContext;

	protected Toast toast = null;

	private String mPageName = DEFAULT_PAGE_NAME;

	private void genmPageName() {
		if (this.mPageName.equals(DEFAULT_PAGE_NAME)) {
			this.mPageName = this.getClass().toString();
		}
		System.out.println("this.mPageName" + this.mPageName);
	};

	public void setmPageName(String mPageName) {
		this.mPageName = mPageName;
	}

	/**
	 * 防止多次点击后toast不消失
	 * 
	 * @param string
	 */
	public void toastShow(String string) {
		if (toast != null) {
			toast.cancel();
		}
		toast = Toast.makeText(getActivity().getApplicationContext(), string,
				Toast.LENGTH_SHORT);
		toast.show();
	}

	protected DBControl db;

	protected void init() {
		genmPageName();
		db = DBControl.getInstance();
	}
}
