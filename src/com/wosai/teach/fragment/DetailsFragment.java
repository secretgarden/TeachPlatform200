package com.wosai.teach.fragment;

import java.util.ArrayList;
import java.util.List;

import com.lidroid.xutils.BitmapUtils;
import com.wosai.teach.R;
import com.wosai.teach.activity.BaseFragmentActivity;
import com.wosai.teach.adapter.DetailsListAdapter;
import com.wosai.teach.entity.DetailsComment;
import com.wosai.teach.entity.User;
import com.wosai.teach.utils.BitmapHelp;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class DetailsFragment extends ListFragment implements OnClickListener{
	private View view;
	private List<DetailsComment> details;
	private DetailsListAdapter adapter = null;
	private ListView listView;
	private Button btComment;
	private User user;
	private ImageView myIcon;
	private BitmapUtils bitmapUtils;
	
	private TextView detailsTitle;

	// R.id.details_title;
	// R.id.details_viewers
	// R.id.details_release
	// R.id.details_type
	// R.id.details_from
	// R.id.details_bt_praise
	// R.id.details_bt_step
	// R.id.details_myicon
	// R.id.details_mycomment
	//
	//
	//
	// details_othericon
	// details_othername
	// details_othercomment_praise
	// details_bt_comment
	// details_othercomment
	// details_othercomment_time

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		init();

	}

	private void init() {
		details = new ArrayList<DetailsComment>();
		DetailsComment de = new DetailsComment();
		DetailsComment de2 = new DetailsComment();
		DetailsComment de3 = new DetailsComment();
		details.add(de);
		details.add(de2);
		details.add(de3);
		adapter = new DetailsListAdapter(getActivity(), details);
		
		setListAdapter(adapter);
		
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		System.out.println("onresume");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.frag_details, container, false);
		listView = (ListView) view.findViewById(android.R.id.list);
		refreshHeight();
		btComment = (Button) view.findViewById(R.id.details_bt_mycomment_ok);
		btComment.setOnClickListener((OnClickListener) this);
		
		user = BaseFragmentActivity.db.findFirst(User.class);
		if(user != null){
			myIcon=(ImageView) view.findViewById(R.id.details_myicon);
			bitmapUtils = BitmapHelp.getBitmapUtils(getActivity());
			bitmapUtils.display(myIcon, user.getIcon1());
		}else{
			System.out.println("user no login!");
		}
		
		return view;
	}

	private void refreshHeight() {
		// 获取ListView对应的Adapter
		int totalHeight = 0;
		for (int i = 0, len = adapter.getCount(); i < len; i++) { // listAdapter.getCount()返回数据项的数目
			View listItem = adapter.getView(i, null, listView);
			listItem.measure(0, 0); // 计算子项View 的宽高
			totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (adapter.getCount() - 1));
		// listView.getDividerHeight()获取子项间分隔符占用的高度
		// params.height最后得到整个ListView完整显示需要的高度
		listView.setLayoutParams(params);

	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		System.out.println("Click On List Item!!!       "+position);
		super.onListItemClick(l, v, position, id);
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.details_bt_mycomment_ok:{
			System.out.println("ok Button clicked!");
			break;
		}
		default:break;
		}
	}
	
}
