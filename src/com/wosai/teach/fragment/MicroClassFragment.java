package com.wosai.teach.fragment;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.wosai.teach.R;
import com.wosai.teach.activity.MicroClassActivity;
import com.wosai.teach.adapter.MicroClassAdapter;
import com.wosai.teach.cst.C;
import com.wosai.teach.dto.JsonResult;
import com.wosai.teach.entity.MicroClass;
import com.wosai.teach.utils.OtherUtils;

public class MicroClassFragment extends AppFragment {
	private View view;
	private List<MicroClass> microClass;
	private MicroClassAdapter adapter = null;
	private ListView listView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.frag_not_name2, null);
		listView = (ListView) view.findViewById(R.id.list);
		listView.setOnItemClickListener(listener);
		initView();
		loadSoftInfo();

		return view;
	}

	private void initView() {

	}

	private void loadSoftInfo() {
		String url = C.HTTP_URL_MICROCLASS;
		System.out.println(url);

		HttpUtils http = new HttpUtils();
		http.send(HttpRequest.HttpMethod.GET, url,
				new RequestCallBack<String>() {

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						JsonResult result = OtherUtils
								.genJsonResult(responseInfo);

						if (result != null && result.getResult() == 0) {

							JsonResult jr = JSON.parseObject(
									responseInfo.result, JsonResult.class);
							microClass = JSON.parseArray(jr.getObject()
									.toString(), MicroClass.class);
							adapter = new MicroClassAdapter(getActivity(),
									microClass);
							listView.setAdapter(adapter);
							
						} else {
						}
					}

					@Override
					public void onFailure(HttpException error, String msg) {
						System.out.println(msg);
						Toast.makeText(getActivity(), "连接失败，请检查网络",
								Toast.LENGTH_SHORT).show();
					}

				});
		if(adapter!=null){
			refreshHeight();
		}
		
	}

	private void refreshHeight() {
		// 获取ListView对应的Adapter
		int totalHeight = 0;
		for (int i = 0, len = adapter.getCount(); i < len; i++) { // listAdapter.getCount()返回数据项的数目
			View listItem = adapter.getView(i, null, listView);
			listItem.measure(0, 0); // 计算子项View 的宽高
			totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (adapter.getCount() - 1));
		// listView.getDividerHeight()获取子项间分隔符占用的高度
		// params.height最后得到整个ListView完整显示需要的高度
		listView.setLayoutParams(params);

	}

	private OnItemClickListener listener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// MicroClass microClass = (MicroClass)
			// parent.getAdapter().getItem(position);
			Intent it = new Intent(getActivity(), MicroClassActivity.class);
			// it.putExtra("expId", microClass.getId());
			startActivity(it);
		}
	};
}
