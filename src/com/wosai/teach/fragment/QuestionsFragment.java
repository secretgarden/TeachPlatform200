package com.wosai.teach.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wosai.teach.R;

public class QuestionsFragment extends Fragment {
	private View view;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.frag_not_name3, null);
		initView();
		loadSoftInfo();
		return view;
	}

	private void initView() {

	}

	private void loadSoftInfo() {

	}
}
