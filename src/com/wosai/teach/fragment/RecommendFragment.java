package com.wosai.teach.fragment;

import java.util.ArrayList;
import java.util.List;

import com.wosai.teach.R;
import com.wosai.teach.adapter.RecommendListAdapter;
import com.wosai.teach.entity.Recommend;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TabHost;

public class RecommendFragment extends ListFragment {
	private View view;
	private List<Recommend> recommend;
	private RecommendListAdapter adapter = null;
	private ListView listView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		recommend = new ArrayList<Recommend>();
		Recommend r1 = new Recommend();
		Recommend r2 = new Recommend();
		Recommend r3 = new Recommend();
		recommend.add(r1);
		recommend.add(r2);
		recommend.add(r3);
		adapter = new RecommendListAdapter(getActivity(), recommend);
		setListAdapter(adapter);


	}

	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.getClass();
		view = inflater.inflate(R.layout.frag_recommend, container, false);
		listView = (ListView) view.findViewById(android.R.id.list);
		refreshHeight();
		return view;
	}

	private void refreshHeight() {
		// 获取ListView对应的Adapter
		int totalHeight = 0;
		for (int i = 0, len = adapter.getCount(); i < len; i++) { // listAdapter.getCount()返回数据项的数目
			View listItem = adapter.getView(i, null, listView);
			listItem.measure(0, 0); // 计算子项View 的宽高
			totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (adapter.getCount() - 1));
		// listView.getDividerHeight()获取子项间分隔符占用的高度
		// params.height最后得到整个ListView完整显示需要的高度
		listView.setLayoutParams(params);

	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		System.out.println("Click On List Item!!!");
		TabHost tabHost = (TabHost) this.getActivity().findViewById(
				R.id.tabhost);
		// TabHost tabHost = (TabHost) v.findViewById(android.R.id.tabhost);
		tabHost.setCurrentTab(0);
		super.onListItemClick(l, v, position, id);
	}
}
