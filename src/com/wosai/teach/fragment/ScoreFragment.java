package com.wosai.teach.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.wosai.teach.R;
import com.wosai.teach.activity.HomeworkAndScoreActivity;
import com.wosai.teach.activity.LoginActivity;
import com.wosai.teach.activity.MainActivity;
import com.wosai.teach.adapter.ScoreAdapter;
import com.wosai.teach.cst.C;
import com.wosai.teach.dto.JsonResult;
import com.wosai.teach.entity.ExpRecUser;
import com.wosai.teach.entity.User;
import com.wosai.teach.utils.OtherUtils;

public class ScoreFragment extends AppFragment {
	private View view;

	private int pageSize = 10;

	private int curPage = 1;

	private User user;

	private ScoreAdapter adapter;

	@ViewInject(R.id.score_list)
	private PullToRefreshListView scoreList;

	@ViewInject(R.id.score_none)
	private LinearLayout scoreNone;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.activity_score, container, false);
		setmPageName("我的成绩");
		init();
		initView();
		loadInfo();
		return view;
	}

	private void initView() {
		scoreList = (PullToRefreshListView) view.findViewById(R.id.score_list);
		scoreNone = (LinearLayout) view.findViewById(R.id.score_none);
		scoreList.setMode(Mode.BOTH);
		List<ExpRecUser> expList = new ArrayList<ExpRecUser>();
		adapter = new ScoreAdapter(getActivity(), expList, db);
		scoreList.setAdapter(adapter);
		scoreList.setOnRefreshListener(new OnRefreshListener2<ListView>() {

			@Override
			public void onPullDownToRefresh(
					final PullToRefreshBase<ListView> refreshView) {
				System.out.println("下拉刷新" + refreshView.getClass());
				curPage = 1;
				loadInfo();
			}

			@Override
			public void onPullUpToRefresh(
					final PullToRefreshBase<ListView> refreshView) {
				System.out.println("上拉加载" + refreshView.getClass());
				curPage++;
				loadInfo();
			}
		});
	}

	private void loadInfo() {
		user = db.findFirst(User.class);
		if (user == null) {
			Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT)
					.show();
			Intent it = new Intent(this.getActivity(), LoginActivity.class);
			startActivity(it);
		} else {

			String url = C.HTTP_URL_EXPERIMENT_REC_LIST;
			url = url.replace("{userId}", user.getUserId() + "");
			url = url.replace("{pageSize}", pageSize + "");
			url = url.replace("{currentPage}", curPage + "");
			HttpUtils http = new HttpUtils();
			http.send(HttpRequest.HttpMethod.GET, url,
					new RequestCallBack<String>() {

						@Override
						public void onSuccess(ResponseInfo<String> responseInfo) {
							if (OtherUtils.CheckNull(responseInfo.result)) {
								return;
							}
							JsonResult jr = JSON.parseObject(
									responseInfo.result, JsonResult.class);
							if (jr.getResult() == 0) {
								System.out.println("当前页||" + curPage);
								if (curPage == 1) {
									// 删除数据库
									db.deleteAll(Selector
											.from(ExpRecUser.class).where(
													"userId", "=",
													user.getUserId()));
								}
								List<ExpRecUser> expList = JSON.parseArray(jr
										.getObject().toString(),
										ExpRecUser.class);
								db.saveAll(expList);
								refreshRecList(expList);
							} else if (curPage == 1) {
								List<ExpRecUser> expList = db.findAll(Selector
										.from(ExpRecUser.class));
								refreshRecList(expList);
							}
							scoreList.onRefreshComplete();
						}

						@Override
						public void onFailure(HttpException error, String msg) {
							if (curPage == 1) {
								List<ExpRecUser> expList = db.findAll(Selector
										.from(ExpRecUser.class));
								refreshRecList(expList);
							}
							scoreList.onRefreshComplete();
						}
					});

		}
	}

	private void refreshRecList(List<ExpRecUser> expList) {
//		expList = new ArrayList<ExpRecUser>();
		if (curPage == 1) {
			adapter.setList(expList);
		} else {
			adapter.addList(expList);
		}
		adapter.notifyDataSetChanged();
		if (adapter.listIsEmpty()) {
			scoreNone.setVisibility(View.VISIBLE);
		} else {
			scoreNone.setVisibility(View.GONE);
		}
	}
}
