package com.wosai.teach.listener;

import com.wosai.teach.view.BannerView;

import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;

public class BannerListener implements OnGestureListener {

	private BannerView bannerView;

	public BannerListener(BannerView bannerView) {
		this.bannerView = bannerView;
	}

	@Override
	public boolean onDown(MotionEvent e) {
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {

	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		System.out.println("BannerListener||" + "onFling");
		final int FLING_MIN_DISTANCE = 0, FLING_MIN_VELOCITY = 0;
		if ((e1.getX() - e2.getX()) > FLING_MIN_DISTANCE
				&& (Math.abs(velocityX)) > FLING_MIN_VELOCITY) {
			System.out.println("onFling==============开始向左滑动了================");
			this.bannerView.showNextView();
			this.bannerView.showNext = true;

		} else if ((e2.getX() - e1.getX()) > FLING_MIN_DISTANCE
				&& (Math.abs(velocityX)) > FLING_MIN_VELOCITY) {
			System.out.println("onFling==============开始向右滑动了================");
			this.bannerView.showPreviousView();
			this.bannerView.showNext = false;
		}
		return false;
	}
}
