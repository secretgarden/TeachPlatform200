package com.wosai.teach.listener;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月1日 下午7:54:59
 * @desc : 检查确定监听
 */
public interface CheckSureListener {
	public void onOk();
}
