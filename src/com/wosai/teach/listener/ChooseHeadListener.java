package com.wosai.teach.listener;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月1日 下午7:54:59
 * @desc : 检查选择拍照
 */
public interface ChooseHeadListener {
	/**
	 * 从本地选取照片
	 */
	public void onNative();

	/**
	 * 拍一张照片
	 */
	public void onPhoto();
}
