package com.wosai.teach.listener;

import java.io.File;

import android.content.Intent;
import android.net.Uri;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.HttpHandler;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.wosai.teach.adapter.SimulationAdapter.ViewHolder;
import com.wosai.teach.cst.C;
import com.wosai.teach.entity.DownloadInfo;
import com.wosai.teach.entity.Experiment;
import com.wosai.teach.fragment.AppFragment;
import com.wosai.teach.service.DownLoadFactory;
import com.wosai.teach.utils.AppGlobal;
import com.wosai.teach.utils.OtherUtils;

public class DownloadRequestCallBack extends RequestCallBack<File> {

	private ViewHolder vh;

	private Experiment exp;

	private AppFragment fragment;

	private DownLoadFactory factory;

	private DownloadInfo downloadInfo;

	private SimuOnItemClickListener listener;

	public DownloadRequestCallBack(ViewHolder vh, DownloadInfo downloadInfo,
			AppFragment fragment, DownLoadFactory factory,
			SimuOnItemClickListener listener, Experiment exp) {
		this.vh = vh;
		this.exp = exp;
		this.fragment = fragment;
		this.factory = factory;
		this.listener = listener;
		this.downloadInfo = downloadInfo;
	}

	private void refreshListItem() {
		// downloadInfo = factory.getDownloadInfo(exp.getExpId() + "");
		refresh();
	}

	private void refresh() {
		vh.showBar();
		if (downloadInfo.getFileLength() > 0) {
			int result = (int) (downloadInfo.getProgress() * 100 / downloadInfo
					.getFileLength());
			vh.rpBar.setProgress(result);
		} else {
			vh.rpBar.setProgress(0);
		}
		HttpHandler.State state = downloadInfo.getState();
		switch (state) {
		case WAITING:
			break;
		case STARTED:
			break;
		case LOADING:
			// 下载中
			break;
		case CANCELLED:
			// 暂停
//			Log.e(this.getClass().toString(), "refresh:CANCELLED");
			break;
		case SUCCESS:
			// 安装
			vh.initVisible();
			Intent intent = new Intent(Intent.ACTION_VIEW);
			String savePath = downloadInfo.getFileSavePath();// apk路径
			if (OtherUtils.CheckNull(savePath)) {
				return;
			}
			intent.setDataAndType(Uri.fromFile(new File(savePath)),
					"application/vnd.android.package-archive");
			fragment.startActivityForResult(intent, C.REQUEST_CODE_INSTALL);
			factory.removeDownload2(downloadInfo);
			listener.refreshDateRel(exp);
			AppGlobal.simulationRefresh = true;
			break;
		case FAILURE:
			// 重新下载
			break;
		}
	}

	@Override
	public void onStart() {
		refreshListItem();
	}

	@Override
	public void onLoading(long total, long current, boolean isUploading) {
//		Log.e("onLoading", "total:" + total + "||current:" + current);
		refreshListItem();
	}

	@Override
	public void onSuccess(ResponseInfo<File> responseInfo) {
		refreshListItem();
	}

	@Override
	public void onFailure(HttpException error, String msg) {
		refreshListItem();
	}

	@Override
	public void onCancelled() {
		refreshListItem();
	}
}