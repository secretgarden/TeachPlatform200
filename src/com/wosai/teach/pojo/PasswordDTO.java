package com.wosai.teach.pojo;

import java.io.Serializable;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月3日 下午1:46:21
 * @desc : 
 */
public class PasswordDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer userId; // 用户ID
	private String loginName; // 登录名
	private String oldPwd; // 旧密码
	private String newPwd; // 新密码

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getOldPwd() {
		return oldPwd;
	}

	public void setOldPwd(String oldPwd) {
		this.oldPwd = oldPwd;
	}

	public String getNewPwd() {
		return newPwd;
	}

	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}

	public PasswordDTO() {
		// TODO Auto-generated constructor stub
	}

	public PasswordDTO(Integer userId, // 用户ID
			String loginName, // 登录名
			String oldPwd, // 旧密码
			String newPwd) {
		this.userId = userId; // 用户ID
		this.loginName = loginName; // 登录名
		this.oldPwd = oldPwd; // 旧密码
		this.newPwd = newPwd; // 新密码
	}

}
