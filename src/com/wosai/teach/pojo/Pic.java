package com.wosai.teach.pojo;

import java.io.Serializable;
import java.util.Date;

public class Pic implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer picId;

	private String picName;

	private String info;

	private Integer wide;

	private Integer high;

	private String url;

	private String position;// 日后可能有多个位置需要图片，此时用该字段区分不同位置的图片。

	private String msModel;// 日后可能需要根据不同的终端类型获取图片，该字段即用于区分终端类型。

	private Integer active;// 这个参数为1时表示图片已激活可供终端展示使用，为0时表示不可用。

	private Date tmCreate;

	private Date tmUpdate;

	private Date tmExpire;

	public Integer getPicId() {
		return picId;
	}

	public void setPicId(Integer picId) {
		this.picId = picId;
	}

	public String getPicName() {
		return picName;
	}

	public void setPicName(String picName) {
		this.picName = picName;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Integer getWide() {
		return wide;
	}

	public void setWide(Integer wide) {
		this.wide = wide;
	}

	public Integer getHigh() {
		return high;
	}

	public void setHigh(Integer high) {
		this.high = high;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getMsModel() {
		return msModel;
	}

	public void setMsModel(String msModel) {
		this.msModel = msModel;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Date getTmCreate() {
		return tmCreate;
	}

	public void setTmCreate(Date tmCreate) {
		this.tmCreate = tmCreate;
	}

	public Date getTmUpdate() {
		return tmUpdate;
	}

	public void setTmUpdate(Date tmUpdate) {
		this.tmUpdate = tmUpdate;
	}

	public Date getTmExpire() {
		return tmExpire;
	}

	public void setTmExpire(Date tmExpire) {
		this.tmExpire = tmExpire;
	}
}
