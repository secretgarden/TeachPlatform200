package com.wosai.teach.service;

import java.util.List;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年8月5日 下午3:44:26
 * @desc : 下载服务
 */
public class DownLoadService extends Service {

	private static DownLoadFactory DOWNLOAD_MANAGER;

	public static DownLoadFactory getDownLoadManager(Context appContext) {
		if (!DownLoadService.isServiceRunning(appContext)) {
			Intent downloadSvr = new Intent(appContext, DownLoadService.class);
			appContext.startService(downloadSvr);
		}
		if (DownLoadService.DOWNLOAD_MANAGER == null) {
			DownLoadService.DOWNLOAD_MANAGER = new DownLoadFactory(appContext);
		}
		return DOWNLOAD_MANAGER;
	}

	public static boolean isServiceRunning(Context context) {
		boolean isRunning = false;
		ActivityManager activityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> serviceList = activityManager
				.getRunningServices(Integer.MAX_VALUE);
		if (serviceList == null || serviceList.size() == 0) {
			return false;
		}

		for (int i = 0; i < serviceList.size(); i++) {
			if (serviceList.get(i).service.getClassName().equals(
					DownLoadService.class.getName())) {
				isRunning = true;
				break;
			}
		}
		return isRunning;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}
