package com.wosai.teach.utils;

import com.wosai.teach.entity.User;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-28 上午11:37:45
 * @desc : 全局变量
 */
public class AppGlobal {
	public static User user;

	public static boolean simulationRefresh = false;
}
