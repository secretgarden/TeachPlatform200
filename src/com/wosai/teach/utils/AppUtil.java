package com.wosai.teach.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.view.KeyEvent;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

public class AppUtil extends Activity {

	private static AppUtil app;

	private Context context;

	private static Toast toast = null;

	private AppUtil() {

	}

	public void init(Context context) {
		this.context = context;
	}

	/**
	 * 获取DBUtil的单例
	 */
	public static AppUtil getInstance() {
		if (app == null) {
			app = new AppUtil();
		}
		return app;
	}

	public void getDB() {

	}

	/**
	 * 获取当前时间，格式为yyyy-MM-dd hh:mm:ss
	 * 
	 * @return
	 */
	public static String getTime() {
		Date today = new Date();
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
				Locale.getDefault());
		return f.format(today);
	}

	/**
	 * 获取当前运行界面的包名
	 * 
	 * @param context
	 * @return
	 */
	public static String getTopPackageName(Context context) {
		ActivityManager activityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		ComponentName cn = activityManager.getRunningTasks(1).get(0).topActivity;
		return cn.getPackageName();
	}

	/**
	 * 获取栈顶activity
	 * 
	 * @param context
	 * @return
	 */
	public static ComponentName getTopActivity(Activity context) {
		ActivityManager manager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> runningTaskInfos = manager.getRunningTasks(1);
		if (runningTaskInfos != null)
			return runningTaskInfos.get(0).topActivity;
		else
			return null;
	}

	/**
	 * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
	 */
	public static int dip2px(Context context, float dpValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dpValue * scale + 0.5f);
	}

	/**
	 * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
	 */
	public static int px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}

	/**
	 * 显示Toast
	 * 
	 * @param context
	 * @param resId
	 */
	public static void showToast(Context context, int resId) {
		if (toast != null) {
			toast.cancel();
		}
		toast = Toast.makeText(context, resId, Toast.LENGTH_SHORT);
		toast.show();
	}

	/**
	 * 显示Toast
	 * 
	 * @param context
	 * @param text
	 */
	public static void showToast(Context context, String text) {
		if (toast != null) {
			toast.cancel();
		}
		toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
		toast.show();
	}

	/**
	 * 获取应用版本信息
	 * 
	 * @param packageName
	 *            包名
	 * @return
	 */
	public static PackageInfo getPackageInfo(String packageName) {
		PackageInfo result = null;
		if (OtherUtils.CheckNull(packageName)) {
			return result;
		}
		List<PackageInfo> packages = AppUtil.getInstance().context
				.getPackageManager().getInstalledPackages(0);
		for (int i = 0; i < packages.size(); i++) {
			PackageInfo packageInfo = packages.get(i);
			if (packageName.equals(packageInfo.packageName)) {
				result = packageInfo;
			}
		}
		return result;
	}

	/**
	 * 版本名字
	 * 
	 * @param packageInfo
	 * @return
	 */
	public static String getVersionName(PackageInfo packageInfo) {
		if (packageInfo != null) {
			return packageInfo.versionName;
		}
		return "未安装";
	}

	/**
	 * 版本编号
	 * 
	 * @param packageInfo
	 * @return
	 */
	public static int getVersionCode(PackageInfo packageInfo) {
		if (packageInfo != null) {
			return packageInfo.versionCode;
		}
		return 0;
	}

	private static long exitTime = 0;

	public static void AppOut(int keyCode, KeyEvent event, Activity activity) {
		if ((System.currentTimeMillis() - exitTime) > 2000) {
			Toast.makeText(activity, "再按一次退出软件", Toast.LENGTH_SHORT).show();
			MobclickAgent.onKillProcess(activity);
			exitTime = System.currentTimeMillis();
		} else {
			ScreenManager.getScreenManager().popAllActivityExceptOne(null);
		}
	}

}
