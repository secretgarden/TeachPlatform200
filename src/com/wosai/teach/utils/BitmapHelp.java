package com.wosai.teach.utils;

import java.io.File;

import android.content.Context;
import android.os.Environment;

import com.lidroid.xutils.BitmapUtils;

/**
 * Author: wyouflf Date: 13-11-12 Time: 上午10:24
 */
public class BitmapHelp {
	private BitmapHelp() {
	}

	private static final String IMAGE_CACHE_PATH = "/teach_platform/image/Cache";

	private static final int READ_TIME_OUT = 60 * 1000;

	private static int memoryCacheSize = 1024 * 1024 * 20; // 20MB

	private static int diskCacheSize = 1024 * 1024 * 100; // 100M

	private static BitmapUtils bitmapUtils;

	/**
	 * BitmapUtils不是单例的 根据需要重载多个获取实例的方法
	 *
	 * @param appContext
	 *            application context
	 * @return
	 */
	public static BitmapUtils getBitmapUtils(Context appContext) {
		if (bitmapUtils == null) {
			String sdcard = Environment.getExternalStorageDirectory().getPath();
			File tmpFile = new File(sdcard + IMAGE_CACHE_PATH);
			if (!tmpFile.exists()) {
				tmpFile.mkdirs();
			}
			bitmapUtils = new BitmapUtils(appContext,
					sdcard + IMAGE_CACHE_PATH, memoryCacheSize, diskCacheSize);
			bitmapUtils.configDefaultReadTimeout(READ_TIME_OUT);
			bitmapUtils.configDefaultConnectTimeout(READ_TIME_OUT);
		}
		return bitmapUtils;
	}
}
