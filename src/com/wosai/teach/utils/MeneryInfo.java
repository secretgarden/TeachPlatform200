package com.wosai.teach.utils;

import java.io.File;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;

@SuppressLint("NewApi")
@SuppressWarnings("deprecation")
public class MeneryInfo {
	/**
	 * 获得SD卡总大小
	 * 
	 * @return
	 */
	public static Integer getSDTotalSize() {
		File path = Environment.getExternalStorageDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = 0, totalBlocks = 0;
		if (Build.VERSION.SDK_INT >= 18) {
			blockSize = stat.getBlockSizeLong();
			totalBlocks = stat.getBlockCountLong();
		} else {
			blockSize = stat.getBlockSize();
			totalBlocks = stat.getBlockCount();
		}
		// return Formatter.formatFileSize(context, blockSize * totalBlocks);
		return (int) (blockSize * totalBlocks / (1024 * 1024));
	}

	/**
	 * 获得sd卡剩余容量，即可用大小
	 * 
	 * @return
	 */
	public static Integer getSDAvailableSize() {
		File path = Environment.getExternalStorageDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = 0, availableBlocks = 0;
		if (Build.VERSION.SDK_INT >= 18) {
			blockSize = stat.getBlockSizeLong();
			availableBlocks = stat.getAvailableBlocksLong();
		} else {
			blockSize = stat.getBlockSize();
			availableBlocks = stat.getAvailableBlocks();
		}
		// return Formatter.formatFileSize(context, blockSize *
		// availableBlocks);
		return (int) (blockSize * availableBlocks / (1024 * 1024));
	}

	/**
	 * 获得机身内存总大小
	 * 
	 * @return
	 */
	public static Integer getRomTotalSize() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = 0, totalBlocks = 0;
		if (Build.VERSION.SDK_INT >= 18) {
			blockSize = stat.getBlockSizeLong();
			totalBlocks = stat.getBlockCountLong();
		} else {
			blockSize = stat.getBlockSize();
			totalBlocks = stat.getBlockCount();
		}
		// return Formatter.formatFileSize(context, blockSize * totalBlocks);
		return (int) (blockSize * totalBlocks / (1024 * 1024));
	}

	/**
	 * 获得机身可用内存
	 * 
	 * @return
	 */
	public static Integer getRomAvailableSize() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = 0, availableBlocks = 0;
		if (Build.VERSION.SDK_INT >= 18) {
			blockSize = stat.getBlockSizeLong();
			availableBlocks = stat.getAvailableBlocksLong();
		} else {
			blockSize = stat.getBlockSize();
			availableBlocks = stat.getAvailableBlocks();
		}
		// return Formatter.formatFileSize(context, blockSize *
		// availableBlocks);
		return (int) (blockSize * availableBlocks / (1024 * 1024));
	}

}
