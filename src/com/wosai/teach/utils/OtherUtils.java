package com.wosai.teach.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;

import android.os.Environment;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.http.ResponseInfo;
import com.wosai.teach.dto.JsonResult;

public class OtherUtils {
	/**
	 * @param list
	 * @return
	 */
	public static boolean CheckNull(List<?> list) {
		if (list == null || list.isEmpty()) {
			return true;
		}
		return false;
	}

	public static boolean CheckNull(Map<?, ?> map) {
		if (map == null || map.isEmpty()) {
			return true;
		}
		return false;
	}

	public static boolean CheckNull(String str) {
		if (str == null) {
			return true;
		} else if ("".equals(str.trim())) {
			return true;
		}
		return false;
	}

	public static boolean CheckNull(Integer it) {
		if (it == null || it == 0) {
			return true;
		}
		return false;
	}

	public static boolean CheckNull(Object[] array) {
		if (array == null || array.length <= 0) {
			return true;
		}
		return false;
	}

	public static boolean CheckIsNumber(String number) {
		if (number == null || number == "") {
			return false;
		}
		for (int i = 0; i < number.length(); i++) {
			if (Character.isDigit(number.charAt(i))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * json转string，转码
	 * 
	 * @param obj
	 * @return
	 */
	public static String JsonToString(Object obj) {
		String json = "";
		if (obj instanceof String) {
			json = Encoder(obj.toString(), "gb2312");
		} else if (obj instanceof JSONArray) {
			json = Encoder(obj.toString(), "gb2312");
		}
		return json;
	}

	public static String Encoder(String str, String Code) {
		String result = "";
		try {
			result = URLEncoder.encode(str, "gb2312");
		} catch (UnsupportedEncodingException e) {
			System.out.println("转码异常");
			e.printStackTrace();
		}
		return result;
	}

	public static JsonResult genJsonResult(ResponseInfo<String> responseInfo) {
		JsonResult result = null;
		if (responseInfo.result != null) {
			result = JSON.parseObject(responseInfo.result, JsonResult.class);
		}
		return result;
	}

	public static boolean hasSdcard() {
		String state = Environment.getExternalStorageState();
		if (state.equals(Environment.MEDIA_MOUNTED)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 验证邮箱
	 * 
	 * @param email
	 * @return matcher.matches()
	 */
	public static boolean isEmailValid(String email) {

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		return matcher.matches();
	}

	/**
	 * 验证QQ
	 * 
	 * @param str
	 * @return boolean true or false
	 */
	public static boolean isQQ(String str) {
		Pattern pattern = Pattern.compile("^[1-9]*[1-9][0-9]*$");
		Matcher m = pattern.matcher(str);
		return m.matches();
	}

	/**
	 * 验证手机号
	 * 
	 * @param mobiles
	 * @return boolean true or false
	 */
	public static boolean isMobileNO(String mobiles) {
		Pattern pattern = Pattern.compile("^(1[3,4,5,7,8][0-9])\\d{8}$");
		Matcher m = pattern.matcher(mobiles);
		return m.matches();
	}

	/**
	 * 
	 * 判定输入汉字
	 * 
	 * @param c
	 * 
	 * @return
	 */

	public static boolean isChinese(char c) {
		Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
				|| ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
				|| ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
				|| ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
				|| ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
				|| ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 检测String是否全是中文
	 * 
	 * @param name
	 * 
	 * @return
	 */

	public static boolean checkNameChese(String name) {

		boolean res = true;
		char[] cTemp = name.toCharArray();
		for (int i = 0; i < name.length(); i++) {
			if (!isChinese(cTemp[i])) {
				res = false;
				break;
			}
		}
		return res;

	}
}
