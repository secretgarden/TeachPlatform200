package com.wosai.teach.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年9月7日 下午4:50:05
 * @desc : 同步相关的工具
 */
public class SynchUtils {

	/**
	 * 间隔时间
	 */
	private static final long interval = 1 * 1000L;

	private static Map<Integer, Long> clickMap = new HashMap<Integer, Long>();

	/**
	 * 控制重复点击问题
	 * 
	 * @param hashCode
	 * @return true,不能点击 false,可以点击
	 */
	public static boolean isFastClick(int hashCode) {
		long time = System.currentTimeMillis();
		Long lastClickTime = clickMap.get(hashCode);
		lastClickTime = lastClickTime == null ? 0 : lastClickTime;
		if (time - lastClickTime < interval) {
			return true;
		}
		clickMap.put(hashCode, time);
		return false;
	}

	private static long lastOpenTime;

	public static boolean canOpen() {
		long time = System.currentTimeMillis();
		if (time - lastOpenTime < 30 * 1000) {
			return false;
		}
		lastOpenTime = time;
		return true;
	}

	public static void initLastOpenTime() {
		lastOpenTime = 0;
	}

	public static void main(String[] args) {
		long time = System.currentTimeMillis();
		clickMap.put(999, 555L);
		Long lastClickTime = clickMap.get(999);
		if (time - lastClickTime < 500) {
			System.out.println("true");
		} else {
			System.out.println("false");
		}
	}
}
