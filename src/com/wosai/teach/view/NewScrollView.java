package com.wosai.teach.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class NewScrollView extends ScrollView {

	public NewScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public NewScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public NewScrollView(Context context) {
		super(context);
	}

	public void setMeasuredDimension1(int measuredWidth, int measuredHeight) {
		setMeasuredDimension(measuredWidth, measuredHeight);
	}
}
